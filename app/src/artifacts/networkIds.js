module.exports = {
    'mainnet': 1,
    'matic': 137,
    'ropsten': 3,
    'rinkeby': 4,
    'goerli': 5,
    'kovan': 42,
    'matictestnet': 80001,
    'ganache': 1337
}