let domain = "http://localhost:5000";

module.exports = {
    CREATE_DISPUTE: domain + "/dispute/create",
    CHALLENGE_DISPUTE: domain + "/dispute/challenge",
    FETCH_DISPUTE: domain + "/disputes",
    GET_DISPUTE_STATUS: domain + "/dispute/status",
    GET_DISPUTE_BY_ORDER_ID: domain + "/dispute",
    GET_DISPUTE_CLAIM_STATUS: domain + "/dispute/status/claim",
    UPDATE_REPUTATION: domain + "/reputation/update",
    GET_REPUTATION_BY_USER: domain + "/reputation"
}