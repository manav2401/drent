import { BrowserRouter as Router, Route, Switch, Link, Redirect } from 'react-router-dom';
import Onboard from './components/onboard/Onboard';
import Login from './components/login/login';
import Profile from './components/login/profile';
import ViewProfile from './components/login/viewprofile';
import Dashboard from './components/dashboard/dashboard';
import ViewProduct from './components/product/viewProduct';
import AddProduct from './components/product/addProduct';
import BorrowProduct from './components/product/borrowProduct';
import Order from './components/order/order';
import ViewOrder from './components/order/vieworder';
import Voting from './components/dispute/voting';
import CreateDispute from './components/dispute/createDispute';
import ChallengeDispute from "./components/dispute/challengeDispute";
import ListDispute from './components/dispute/listDispute';
import ViewDispute from './components/dispute/viewdispute';
import Test from './components/tests/tests';
import ERC20Test from './components/tests/erc20test';

import './App.css';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          {/* <Route exact path="/" component={Onboard} /> */}
          <Route exact path="/app" component={Dashboard} />
          <Route exact path="/app/login" component={Login} />
          <Route exact path="/app/profile" component={Profile} />
          <Route exact path="/app/profile/:address" component={ViewProfile} />
          <Route excat path="/app/products/:productId" component={ViewProduct} />
          <Route exact path="/app/addproduct" component={AddProduct} />
          <Route exact path="/app/borrowproduct" component={BorrowProduct} />
          <Route exact path="/app/orders" component={Order} />
          <Route exact path="/app/orders/:orderId" component={ViewOrder} />
          <Route exact path="/voting" component={Voting} />
          <Route exact path="/app/dispute/create" component={CreateDispute}/>
          <Route exact path="/app/dispute/challenge" component={ChallengeDispute}/>
          <Route exact path="/app/orders/:orderId/dispute" component={ListDispute}/>
          <Route exact path="/app/disputes" component={ViewDispute}/>
          <Route exact path="/app/test" component={Test}/>
          <Route exact path="/app/erc20test" component={ERC20Test}/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
