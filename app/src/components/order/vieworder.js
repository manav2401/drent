import React, { useState, useEffect, Fragment } from 'react';
import { Link as RouterLink, useParams, useHistory } from 'react-router-dom';
import {
    Button,
    Paper,
    Typography,
} from '@material-ui/core';
import LaunchIcon from '@material-ui/icons/Launch';
import DateFnsUtils from '@date-io/date-fns';
import 'date-fns';
import axios from 'axios';

import Header from '../common/header';
import { useStyles } from '../common/styles';

function ViewOrder() {

    const classes = useStyles();

    // get path params
    const params = useParams();

    // get history
    const history = useHistory();

    // user wallet address
    const [address, setAddress] = useState(null);
    const [provider, setProvider] = useState(null);

    // user
    const [orderHash, setOrderHash] = useState(params.orderId);
    const [order, setOrder] = useState(null);
    const [startDate, setStartDate] = useState(null);
    const [endDate, setEndDate] = useState(null);

    useEffect(async () => {

        if (provider && orderHash) {

            try {

                const url = "https://ipfs.io/ipfs/" + String(orderHash);

                axios.get(url).then(response => {

                    // set the user details from object
                    response = response.data;

                    // validations
                    if (response.hasOwnProperty("ownerAddress") &&
                        response.hasOwnProperty("borrowerAddress") &&
                        response.hasOwnProperty("productId") &&
                        response.hasOwnProperty("startDate") &&
                        response.hasOwnProperty("endDate") &&
                        response.hasOwnProperty("disputeCreated") &&
                        response.hasOwnProperty("rentAmount") &&
                        response.hasOwnProperty("collateralAmount")) {

                        setOrder(response);

                        const startDate = new Date(response.startDate * 1000);
                        const endDate = new Date(response.endDate * 1000);

                        setStartDate(startDate.getDate() + " / " + startDate.getMonth() + " / " + startDate.getFullYear());
                        setEndDate(endDate.getDate() + " / " + endDate.getMonth() + " / " + endDate.getFullYear());

                    }

                }).catch(error => {
                    console.log("Error in fetching order from hash:", error);
                })

            } catch (error) {
                // no profile exists
                console.log("Error in fetching profile:", error);
            }

        }

    }, [provider]);

    return (
        <div>

            <Header
                setParentAddress={(address) => { setAddress(address) }}
                setEthersProvider={(provider) => { setProvider(provider) }}
            />

            {address
                ?
                (order)
                    ?
                    <React.Fragment>
                        {/* <form noValidate autoComplete="off"> */}
                        <main className={classes.layout}>
                            <div className={classes.loadingbar}>
                                {/* {loading ? <LinearProgress /> : null} */}
                            </div>

                            <Paper className={classes.paper}>

                                <div style={{ justifyContent: 'space-evenly' }} className={classes.sidediv}>

                                    {/* Order Details */}
                                    <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                                        <div className={classes.sidediv} style={{ justifyContent: 'space-between' }}>
                                            <div className={classes.updiv}>
                                                <div>Product: </div>
                                                <div>Owner: </div>
                                                <div>Borrower: </div>
                                                <div>Start Date: </div>
                                                <div>End Date: </div>
                                                <div>Total Rent: </div>
                                                <div>Collateral: </div>
                                            </div>
                                            <div className={classes.updiv}>
                                                <div>
                                                    {order.productId}
                                                    <RouterLink to={`/app/products/${order.productId}`}>
                                                        <LaunchIcon fontSize="small" />
                                                    </RouterLink>
                                                </div>
                                                <div>
                                                    {order.ownerAddress}
                                                    <RouterLink to={`/app/profile/${order.ownerAddress}`}>
                                                        <LaunchIcon fontSize="small" />
                                                    </RouterLink>
                                                </div>
                                                <div>
                                                    {order.borrowerAddress}
                                                    <RouterLink to={`/app/profile/${order.borrowerAddress}`}>
                                                        <LaunchIcon fontSize="small" />
                                                    </RouterLink>
                                                </div>
                                                <div>{startDate}</div>
                                                <div>{endDate}</div>
                                                <div>{order.rentAmount} DAI</div>
                                                <div>{order.collateralAmount} DAI</div>
                                            </div>
                                        </div>
                                    </Typography>
                                </div>

                            </Paper>

                            <div className={classes.sidediv}>
                                <RouterLink to='/app'>
                                    <Button
                                        type="submit"
                                        variant="outlined"
                                        color="primary"
                                        className={classes.hspace}
                                    // onClick={(e) => { onProfileSave(e) }}
                                    >
                                        Go To Dashboard
                                    </Button>
                                </RouterLink>
                            </div>
                        </main>
                        {/* </form> */}
                    </React.Fragment>
                    :
                    <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                        <div>
                            No order found
                        </div>
                        <div className={classes.sidediv}>
                            <RouterLink to='/app'>
                                <Button
                                    type="submit"
                                    variant="outlined"
                                    color="primary"
                                    className={classes.hspace}
                                    to={'/app'}
                                // onClick={(e) => { onProfileSave(e) }}
                                >
                                    Go To Dashboard
                                </Button>
                            </RouterLink>
                        </div>
                    </Typography>
                :
                <Fragment>
                    <div className={classes.loadingbar}></div>
                    <main className={classes.layout}>
                        <Paper className={classes.paper}>
                            <Typography component="h1" variant="h4" align="center">
                                Please connect with a wallet to continue
                            </Typography>
                        </Paper>
                    </main>
                </Fragment>
            }

        </div>
    );

}

export default ViewOrder;