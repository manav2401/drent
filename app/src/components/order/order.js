import React, { useState, useEffect, Fragment } from 'react';
import { Link, Link as RouterLink, useHistory } from 'react-router-dom';
import {
    TextField,
    Grid,
    Button,
    IconButton,
    Paper,
    Typography,
    Snackbar,
    Card,
    CardContent,
    CardMedia,
    Container,
    Modal
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import MenuIcon from '@material-ui/icons/Menu';
import { ethers } from 'ethers';
import axios from 'axios';

import Header from '../common/header';
import { useStyles } from '../common/styles';
import { drentContractAddress, drentContractAbi } from '../../artifacts/DrentContract';
import { GET_DISPUTE_STATUS } from '../../artifacts/uris'

const IpfsHttpClient = require('ipfs-http-client')

function getModalStyle() {
    return {
        top: '50%',
        left: '50%',
        transform: `translate(-50%, -50%)`,
    };
}

function Order() {

    // use styles
    const classes = useStyles();
    const [modalStyle] = React.useState(getModalStyle);

    // use history
    const history = useHistory();

    // user wallet address
    const [address, setAddress] = useState(null);
    const [provider, setProvider] = useState(null);
    const [contract, setContract] = useState(null);

    // ipfs client
    const [ipfsClient, setIpfsClient] = useState(null);

    // order
    const [orders, setOrders] = useState([]);
    const [orderHashes, setOrderHashes] = useState([]);
    // const [disputesByOrder, setDisputesByOrder] = useState([]);

    const tempOrder = {
        ownerAddress: "ownerAddress",
        borrowerAddress: "borrowerAddress",
        productId: "productId",
        startDate: "startDate",
        endDate: "timestamp",
        disputeCreated: false,
        rentAmount: "rentAmount",
        collateralAmount: "collateralAmount"
    }

    useEffect(async () => {

        if (provider && address) {

            // console.log("address:", address);

            const ipfsClient = new IpfsHttpClient({
                url: new URL('https://ipfs.infura.io:5001')
            });

            setIpfsClient(ipfsClient);

            // get signer
            const signer = provider.getSigner();

            // create contract instance
            const drentContract = new ethers.Contract(drentContractAddress, drentContractAbi, signer);
            setContract(drentContract);

            let ordersHash = null;
            try {
                ordersHash = await drentContract.getUserOrders();
            } catch (error) {
                console.log("Unable to fetch orders:", error);
            }

            if (ordersHash) {

                setOrderHashes(ordersHash);

                let orderObject = [] // new Array(orderObject.length);
                // let disputesObject = [];
                let url = "";

                for (let i = 0; i < ordersHash.length; i++) {

                    url = "https://ipfs.io/ipfs/" + ordersHash[i].toString();

                    axios.get(url).then(response => {

                        let resp = response.data;
                        resp.hash = ordersHash[i].toString();

                        const options = {
                            method: "POST",
                            url: GET_DISPUTE_STATUS,
                            data: {
                                orderId: ordersHash[i],
                                address: address,
                            }
                        }

                        axios(options).then(response => {

                            resp.dispute = response.data;

                            const productUrl = "https://ipfs.io/ipfs/" + resp.productId;

                            axios(productUrl).then(response => {

                                // console.log("i=", i, ":", response.data.name);

                                resp.productName = response.data.name;

                                orderObject.push(resp);

                                if (orderObject.length == ordersHash.length) {
                                    // console.log(orderObject);
                                    setOrders(orderObject);
                                }

                            })

                        }).catch(error => {

                            resp.dispute = {
                                message: "Create Dispute",
                                created: false,
                                challenged: false,
                                disabled: true
                            }

                            const productUrl = "https://ipfs.io/ipfs/" + resp.productId;

                            axios(productUrl).then(response => {

                                resp.productName = response.data.name;

                                if (orderObject.length == ordersHash.length) {
                                    setOrders(orderObject);
                                }

                            })


                        })

                    }).catch(error => {
                        console.log(error);
                    });
                }

            }

        }

    }, [provider, address]);


    const handleDisputeButtonClick = (index) => {

        if (orders[index].dispute.created == false) {
            history.push("/app/dispute/create", {
                orderId: orderHashes[index],
                initiatorAddress: address,
                oppositionAddress: orders[index].borrowerAddress == address ? orders[index].ownerAddress : orders[index].borrowerAddress,
            })
        } else {
            if (orders[index].dispute.challenged == false) {
                history.push("/app/dispute/challenge", {
                    orderId: orderHashes[index],
                });
            } else {
                history.push("/app/orders/" + orderHashes[index] + "/dispute", {
                    orderId: orderHashes[index],
                });
            }
        }

        // let arr = [...orders];
        // arr[index].disputeCreated = true;
        // setOrders(arr);
    }

    return (
        <div>

            <Header
                setParentAddress={(address) => { setAddress(address) }}
                setEthersProvider={(provider) => { setProvider(provider) }}
            />

            {address
                ?
                (orders.length != 0)
                    ?
                    <div className={classes.vspace}>
                        <Container maxWidth="md">
                            <Grid container spacing={4}>
                                {orders.map((order, i) => (
                                    { order }
                                        ?
                                        <Grid item key={order.hash} xs={12} sm={6} md={6}>
                                            <div>
                                                <Card className={classes.card}>
                                                    <CardContent className={classes.cardContent}>
                                                        <Typography gutterBottom variant="h5" component="h2">
                                                            {order.productName}
                                                        </Typography>
                                                        <Typography>
                                                            Total Rent: {order.rentAmount} DAI / day
                                                        </Typography>
                                                        <Typography>
                                                            Total Collateral: {order.collateralAmount} DAI
                                                        </Typography>
                                                    </CardContent>

                                                    <div className={classes.sidediv}>
                                                        <div className={classes.submit}>
                                                            <RouterLink to={`/app/orders/${order.hash}`}>
                                                                <Button variant="contained" color="primary" className={classes.hspace}>
                                                                    Show More
                                                                </Button>
                                                            </RouterLink>
                                                        </div>
                                                        <div className={classes.submit}>
                                                            {order.dispute
                                                                ?
                                                                <Button disabled={order.dispute.disabled} variant="contained" color="primary" className={classes.hspace} onClick={() => { handleDisputeButtonClick(i) }}>
                                                                    {order.dispute.message}
                                                                </Button>
                                                                :
                                                                <div></div>
                                                            }
                                                        </div>
                                                    </div>

                                                </Card>
                                            </div>
                                        </Grid>
                                        :
                                        <div></div>
                                ))}

                            </Grid>
                        </Container>
                    </div>
                    :
                    <div>
                        <Typography component="h6" variant="h6" align="center">
                            <div>
                                No orders to display...Why not try borrowing a product?
                        </div>
                        </Typography>
                        <div className={classes.sidediv}>
                            <div className={classes.submit}>
                                <RouterLink to='/app'>
                                    <Button
                                        type="submit"
                                        variant="outlined"
                                        color="primary"
                                        className={classes.hspace}
                                    >
                                        Go to Dashboard
                                    </Button>
                                </RouterLink>
                            </div>
                            <div className={classes.submit}>
                                <RouterLink to='/app/addproduct'>
                                    <Button
                                        type="submit"
                                        variant="outlined"
                                        color="primary"
                                        className={classes.hspace}
                                    >
                                        Add a product on rent
                                    </Button>
                                </RouterLink>
                            </div>
                        </div>
                    </div>

                :
                <Fragment>
                    <div className={classes.loadingbar}></div>
                    <main className={classes.layout}>
                        <Paper className={classes.paper}>
                            <Typography component="h1" variant="h4" align="center">
                                Please connect with a wallet to continue
                            </Typography>
                        </Paper>
                    </main>
                </Fragment>
            }
        </div>
    );

}

export default Order;