import { useState, useEffect } from 'react';
import { Fragment } from 'react';
import { AppBar, Button, Checkbox, IconButton, Paper, Toolbar, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import Header from '../common/header';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    topbar: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between"
    },
    sidediv: {
        display: "flex",
        flexDirection: "row",
        padding: theme.spacing(2)
    },
    updiv: {
        display: "flex",
        flexDirection: "column",
        padding: theme.spacing(2)
    },
    address: {
        padding: theme.spacing(2)
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(10),
        marginRight: theme.spacing(10),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
            width: 'auto',
            marginLeft: theme.spacing(10),
            marginRight: theme.spacing(10),
        },
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
    },
    spacing: {
        padding: theme.spacing(3, 0, 3),
        spacing: 2
    }
}));

function Login() {

    const classes = useStyles();
    const [address, setAddress] = useState(null);

    return (
        <div>
            <Header setParentAddress = {(address) => {setAddress(address)}}/>
            {address
                ?
                <Fragment>
                    <main className={classes.layout}>
                        <Paper className={classes.paper}>
                            <Typography component="h1" variant="h4" align="center">
                                {/* <Checkbox checked={true} style={{color: "green"}}>
                                </Checkbox> */}
                                Welcome to DRent!
                            </Typography>
                            <Typography className={classes.spacing}>
                                <Button variant="outlined">
                                    Complete Profile
                                </Button>
                                <Button variant="outlined">
                                    Skip for now
                                </Button>
                            </Typography>
                        </Paper>
                    </main>
                </Fragment>
                :
                <Fragment>
                    <main className={classes.layout}>
                        <Paper className={classes.paper}>
                            <Typography component="h1" variant="h4" align="center">
                                Please connect with a wallet to continue
                            </Typography>
                        </Paper>
                    </main>
                </Fragment>
            }
        </div>
    );

}

export default Login;