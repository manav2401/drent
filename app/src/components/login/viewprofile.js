import React, { useState, useEffect, Fragment } from 'react';
import { Link as RouterLink, useParams, useHistory } from 'react-router-dom';
import {
    TextField,
    Grid,
    AppBar,
    Button,
    Checkbox,
    IconButton,
    Paper,
    Toolbar,
    Typography,
    LinearProgress,
    Snackbar
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import MenuIcon from '@material-ui/icons/Menu';
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
} from '@material-ui/pickers';
// import MomentUtils from '@date-io/moment';
import DateFnsUtils from '@date-io/date-fns';
import 'date-fns';

import { ethers } from 'ethers';
import axios from 'axios';

import Header from '../common/header';
import { useStyles } from '../common/styles';
import { drentContractAddress, drentContractAbi } from '../../artifacts/DrentContract';
import { GET_REPUTATION_BY_USER } from "../../artifacts/uris";

const IpfsHttpClient = require('ipfs-http-client')

function ViewProfile() {

    const classes = useStyles();

    // get path params
    const params = useParams();

    // get history
    const history = useHistory();

    // user wallet address
    const [address, setAddress] = useState(null);
    const [provider, setProvider] = useState(null);

    // user
    const [userAddress, setUserAddress] = useState(params.address);
    const [user, setUser] = useState(null);
    const [reputation, setReputation] = useState(null);

    useEffect(async () => {

        if (provider && userAddress) {

            try {

                const contract = new ethers.Contract(drentContractAddress, drentContractAbi, provider);

                let params = {
                    from: String(userAddress).toLowerCase(),
                };

                let userHash = await contract.getUser(params);

                const url = "https://ipfs.io/ipfs/" + String(userHash);

                axios.get(url).then(response => {

                    // set the user details from object
                    response = response.data;

                    // validations
                    if (response.hasOwnProperty("address") &&
                        response.hasOwnProperty("name") &&
                        response.hasOwnProperty("email") &&
                        response.hasOwnProperty("phone") &&
                        response.hasOwnProperty("street1") &&
                        response.hasOwnProperty("street2") &&
                        response.hasOwnProperty("city") &&
                        response.hasOwnProperty("state")) {

                        setUser(response);

                    }

                    // get reputation
                    const options = {
                        method: "GET",
                        url: GET_REPUTATION_BY_USER,
                        params: {
                            address: String(userAddress).toLowerCase()
                        }
                    }

                    axios(options).then(response => {
                        setReputation(response.data.reputation);
                    }).catch(error => {
                        console.log("Error in fetching reputation", error);
                    })

                }).catch(error => {
                    console.log("Error in fetching user from hash:", error);
                })

            } catch (error) {
                // no profile exists
                console.log("Error in fetching profile:", error);
            }

        }

    }, [provider]);

    return (
        <div>

            <Header
                setParentAddress={(address) => { setAddress(address) }}
                setEthersProvider={(provider) => { setProvider(provider) }}
            />

            {address
                ?
                (user)
                    ?
                    <React.Fragment>
                        {/* <form noValidate autoComplete="off"> */}
                        <main className={classes.layout}>
                            <div className={classes.loadingbar}>
                                {/* {loading ? <LinearProgress /> : null} */}
                            </div>

                            <Paper className={classes.paper}>

                                <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                                    <div>Address: {user.address}</div>
                                </Typography>

                                <div style={{ justifyContent: 'space-evenly' }} className={classes.sidediv}>

                                    {/* Dispute Details */}
                                    <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                                        <div className={classes.sidediv} style={{ justifyContent: 'space-between' }}>
                                            <div className={classes.updiv}>
                                                <div>Name: </div>
                                                <div>Email: </div>
                                                <div>Phone: </div>
                                                <div>Street 1: </div>
                                                <div>Street 2: </div>
                                                <div>City: </div>
                                                <div>State: </div>
                                                <div>Reputation: </div>
                                            </div>
                                            <div className={classes.updiv}>
                                                <div>{user.name}</div>
                                                <div>{user.email}</div>
                                                <div>{user.phone}</div>
                                                <div>{user.street1}</div>
                                                <div>{user.street2}</div>
                                                <div>{user.city}</div>
                                                <div>{user.state}</div>
                                                <div>{reputation ? reputation : 0}</div>
                                            </div>
                                        </div>
                                    </Typography>
                                </div>

                            </Paper>

                            <div className={classes.sidediv}>
                                <RouterLink to='/app'>
                                    <Button
                                        type="submit"
                                        variant="outlined"
                                        color="primary"
                                        className={classes.hspace}
                                    // onClick={(e) => { onProfileSave(e) }}
                                    >
                                        Back to Dashboard
                                    </Button>
                                </RouterLink>
                            </div>
                        </main>
                        {/* </form> */}
                    </React.Fragment>
                    :
                    <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                        <div>
                            No profile found
                        </div>
                        <div className={classes.sidediv}>
                            <RouterLink to='/app'>
                                <Button
                                    type="submit"
                                    variant="outlined"
                                    color="primary"
                                    className={classes.hspace}
                                    to={'/app'}
                                // onClick={(e) => { onProfileSave(e) }}
                                >
                                    Go To Dashboard
                                </Button>
                            </RouterLink>
                        </div>
                    </Typography>
                :
                <Fragment>
                    <div className={classes.loadingbar}></div>
                    <main className={classes.layout}>
                        <Paper className={classes.paper}>
                            <Typography component="h1" variant="h4" align="center">
                                Please connect with a wallet to continue
                            </Typography>
                        </Paper>
                    </main>
                </Fragment>
            }

        </div>
    );

}

export default ViewProfile;