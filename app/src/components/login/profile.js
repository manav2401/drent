import React, { useState, useEffect, Fragment } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import {
    TextField,
    Grid,
    AppBar,
    Button,
    Checkbox,
    IconButton,
    Paper,
    Toolbar,
    Typography,
    LinearProgress,
    Snackbar
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import MenuIcon from '@material-ui/icons/Menu';
import { ethers } from 'ethers';
import axios from 'axios';
import { Biconomy } from "@biconomy/mexa";

import Header from '../common/header';
import { useStyles } from '../common/styles';
import { drentContractAddress, drentContractAbi } from '../../artifacts/DrentContract';
import { pinIpfs } from '../../utils/pinIpfs';
import { GET_REPUTATION_BY_USER } from "../../artifacts/uris";

const IpfsHttpClient = require('ipfs-http-client')

function Profile() {

    // use styles
    const classes = useStyles();

    // user wallet address
    const [address, setAddress] = useState(null);
    const [provider, setProvider] = useState(null);
    const [contract, setContract] = useState(null);
    const [biconomy, setBiconomy] = useState(null);

    // ipfs client
    const [ipfsClient, setIpfsClient] = useState(null);

    // profile details
    const [name, setName] = useState(null);
    const [email, setEmail] = useState(null);
    const [phone, setPhone] = useState(null);
    const [street1, setStreet1] = useState(null);
    const [street2, setStreet2] = useState(null);
    const [city, setCity] = useState(null);
    const [state, setState] = useState(null);
    const [user, setUser] = useState(null);
    const [userHash, setUserHash] = useState(null);
    const [reputation, setReputation] = useState(null);

    // other params
    const [loading, setLoading] = useState(false);
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState(null);

    useEffect(async () => {

        if (provider && address) {

            const ipfsClient = new IpfsHttpClient({
                url: new URL('https://ipfs.infura.io:5001')
            });

            setIpfsClient(ipfsClient);

            // get signer
            const signer = provider.getSigner();

            // create contract instance
            const drentContract = new ethers.Contract(drentContractAddress, drentContractAbi, signer);
            setContract(drentContract);

            console.log("Address:", address);
            console.log("Provider:", provider);

            // check if the profile exists or not
            let userProfile = null;
            try {

                userProfile = await drentContract.getUser();

            } catch (error) {
                // no profile exists
                console.log("Error in fetching profile:", error);
            }

            try {
                
                // instantiate biconomy
                const biconomy = new Biconomy(provider,
                    { apiKey: "MuxSIxgAM.902471e3-88ed-4d64-8d8b-8b81d368fc67" }
                );

                biconomy.onEvent(biconomy.READY, async () => {

                    console.log("Biconomy Initialized..");
                    setBiconomy(biconomy);

                }).onEvent(biconomy.ERROR, (error, message) => {
                    console.log("Error in biconomy initialization")
                    console.log("Error:", error, "Message", message);
                })

            } catch (error) {
                // error in biconomy
                console.log("error in biconomy:", error);
            }

            if (userProfile) {

                // fetch the user object
                const url = "https://ipfs.io/ipfs/" + String(userProfile);
                axios.get(url).then(response => {

                    // set the user details from object
                    response = response.data;

                    setName(response.name);
                    setEmail(response.email);
                    setPhone(response.phone);
                    setStreet1(response.street1);
                    setStreet2(response.street2);
                    setCity(response.city);
                    setState(response.state);

                    const options = {
                        method: "GET",
                        // url: GET_DISPUTE_CLAIM_STATUS + "?orderId=" + String(orderId) + "&address=" + String(address),
                        url: GET_REPUTATION_BY_USER,
                        params: {
                            address: String(address)
                        }
                    }

                    axios(options).then(response => {
                        setReputation(response.data.reputation);
                    }).catch(error => {
                        console.log("Error in fetching reputation", error);
                    })

                }).catch(error => {
                    console.log("Error in fetching user from hash:", error);
                })

            }

        }

    }, [provider, address]);

    const handleNameChange = (e) => {
        setName(e.target.value);
    }

    const handleEmailChange = (e) => {
        setEmail(e.target.value);
    }

    const handlePhoneChange = (e) => {
        setPhone(e.target.value);
    }

    const handleStreet1Change = (e) => {
        setStreet1(e.target.value);
    }

    const handleStreet2Change = (e) => {
        setStreet2(e.target.value);
    }

    const handleCityChange = (e) => {
        setCity(e.target.value);
    }

    const handleStateChange = (e) => {
        setState(e.target.value);
    }

    const onProfileSave = async (e) => {

        e.preventDefault();

        // create json data
        let user = {
            'address': address,
            'name': name,
            'email': email,
            'phone': phone,
            'street1': street1,
            'street2': street2,
            'city': city,
            'state': state
        };
        setUser(user);
        setLoading(true);

        // send data to ipfs
        try {

            const { cid } = await ipfsClient.add(JSON.stringify(user));

            if (cid.string) {

                setUserHash(cid.string);

                if (biconomy) {

                    // create contract instance with biconomy signer
                    let drentContract = new ethers.Contract(drentContractAddress, drentContractAbi, biconomy.getSignerByAddress(address));

                    // populate transaction
                    let { data } = await drentContract.populateTransaction.addUser(cid.string);

                    // get biconomy provider
                    let biconomyProvider = biconomy.getEthersProvider();

                    let gasLimit = await provider.estimateGas({
                        to: drentContractAddress,
                        from: address,
                        data: data
                    });

                    let txParams = {
                        data: data,
                        to: drentContractAddress,
                        from: address,
                        gasLimit: gasLimit, // optional
                        signatureType: "EIP712_SIGN"
                    };

                    // as ethers does not allow providing custom options while sending transaction
                    let tx = await biconomyProvider.send("eth_sendTransaction", [txParams]);

                    //event emitter methods
                    provider.once(tx, async (transaction) => {
                        // Emitted when the transaction has been mined
                        //show success message
                        // console.log(transaction);
                        // pin hash
                        console.log("Hello world...")
                        await pinIpfs(cid.string);
                        setSnackbarMessage("Profile Saved");
                        setSnackbarOpen(true);
                        setLoading(false);
                    });

                } else {

                    // biconomy not initialized
                    console.log("error in biconomy")
                    setSnackbarOpen(true);
                    setSnackbarMessage("Unable to save profile. Try again later.");
                    setLoading(false);

                }

            } else {

                // unable to save in ipfs
                console.log("error in ipfs")
                setSnackbarOpen(true);
                setSnackbarMessage("Unable to save profile. Try again later.");
                setLoading(false);

            }

        } catch (error) {

            console.log("Error in saving profile. Try later", error);
            setSnackbarMessage("Unable to save profile. Try again later.");
            setSnackbarOpen(true);
            setLoading(false);

        }

    }

    // handle snackbar close
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnackbarOpen(false);
    };

    const ProfileForm = (
        <React.Fragment>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <TextField
                        required
                        id="name"
                        name="name"
                        label="Full Name"
                        fullWidth
                        value={name || ''}
                        onChange={handleNameChange}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        required
                        id="email"
                        name="email"
                        label="Email Address"
                        fullWidth
                        value={email || ''}
                        onChange={handleEmailChange}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        required
                        id="phone"
                        name="phone"
                        label="Contact Number"
                        fullWidth
                        value={phone || ''}
                        onChange={handlePhoneChange}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        required
                        id="street1"
                        name="street1"
                        label="Street 1"
                        fullWidth
                        value={street1 || ''}
                        onChange={handleStreet1Change}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        required
                        id="street2"
                        name="street2"
                        label="Street 2"
                        fullWidth
                        value={street2 || ''}
                        onChange={handleStreet2Change}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        required
                        id="city"
                        name="city"
                        label="City"
                        fullWidth
                        autoComplete="city"
                        value={city || ''}
                        onChange={handleCityChange}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        required
                        id="state"
                        name="state"
                        label="State/Province/Region"
                        fullWidth
                        autoComplete="state"
                        value={state || ''}
                        onChange={handleStateChange}
                    />
                </Grid>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    // className={classes.submit}
                    className={classes.button}
                    onClick={(e) => { onProfileSave(e) }}
                >
                    Save
                </Button>
            </Grid>
        </React.Fragment>
    )

    return (
        <div>
            <Header
                setParentAddress={(address) => { setAddress(address) }}
                setEthersProvider={(provider) => { setProvider(provider) }}
            />

            {address
                ?
                <React.Fragment>
                    <form noValidate autoComplete="off">
                        <main className={classes.layout}>
                            <div className={classes.loadingbar}>
                                {loading ? <LinearProgress /> : null}
                            </div>
                            <Paper className={classes.paper}>
                                <Typography component="h1" variant="h4" align="center">
                                    <div>
                                        {address ? ('Hello ' + address) : ''}
                                    </div>
                                </Typography>
                                <React.Fragment>
                                    {ProfileForm}
                                </React.Fragment>
                            </Paper>
                            <div className={classes.sidediv}>
                                <div className={classes.submit}>
                                    <RouterLink to='/app'>
                                        <Button
                                            type="submit"
                                            variant="outlined"
                                            color="primary"
                                            className={classes.hspace}
                                            to={'/app'}
                                        // onClick={(e) => { onProfileSave(e) }}
                                        >
                                            Go to Dashboard
                                        </Button>
                                    </RouterLink>
                                </div>
                                <div className={classes.submit}>
                                    <RouterLink to='/app/addproduct'>
                                        <Button
                                            type="submit"
                                            variant="outlined"
                                            color="primary"
                                            className={classes.hspace}
                                        // onClick={(e) => { onProfileSave(e) }}
                                        >
                                            Add a product on rent
                                        </Button>
                                    </RouterLink>
                                </div>
                            </div>
                        </main>
                    </form>
                </React.Fragment>
                :
                <Fragment>
                    <div className={classes.loadingbar}></div>
                    <main className={classes.layout}>
                        <Paper className={classes.paper}>
                            <Typography component="h1" variant="h4" align="center">
                                Please connect with a wallet to continue
                            </Typography>
                        </Paper>
                    </main>
                </Fragment>
            }

            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                open={snackbarOpen}
                autoHideDuration={4000}
                onClose={handleClose}
                message={snackbarMessage}
                action={
                    <React.Fragment>
                        <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                            <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment>
                }
            />


        </div>

    );

}

export default Profile;