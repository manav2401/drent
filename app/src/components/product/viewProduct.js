import React, { useState, useEffect, Fragment } from 'react';
import { Link as RouterLink, useParams, useHistory } from 'react-router-dom';
import {
    TextField,
    Grid,
    AppBar,
    Button,
    Checkbox,
    IconButton,
    Paper,
    Toolbar,
    Typography,
    LinearProgress,
    Snackbar
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import MenuIcon from '@material-ui/icons/Menu';
import LaunchIcon from '@material-ui/icons/Launch';
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
} from '@material-ui/pickers';
// import MomentUtils from '@date-io/moment';
import DateFnsUtils from '@date-io/date-fns';
import 'date-fns';

import { ethers } from 'ethers';
import axios from 'axios';

import Header from '../common/header';
import { useStyles } from '../common/styles';
import { drentContractAddress, drentContractAbi } from '../../artifacts/DrentContract';

const IpfsHttpClient = require('ipfs-http-client')

function ViewProduct() {

    const classes = useStyles();

    // get path params
    const params = useParams();

    // get history
    const history = useHistory();

    // user wallet address
    const [address, setAddress] = useState(null);
    const [provider, setProvider] = useState(null);

    // ipfs client
    const [ipfsClient, setIpfsClient] = useState(null);

    // product
    const [product, setProduct] = useState(null);
    const [productHash, setProductHash] = useState(params.productId);
    const [isAvailable, setIsAvailable] = useState(null);
    const [startIndex, setStartIndex] = useState(null);
    const [endIndex, setEndIndex] = useState(null);

    // other params
    const [loading, setLoading] = useState(false);
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState(null);

    // date range
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [availabilityMessage, setAvailabilityMessage] = useState(null);
    const [isValid, setIsValid] = useState(false);

    useEffect(async () => {

        if (provider && productHash) {

            // set the ipfs client
            const ipfsClient = new IpfsHttpClient({
                url: new URL('https://ipfs.infura.io:5001')
            });

            setIpfsClient(ipfsClient);

            const url = "https://ipfs.io/ipfs/" + productHash;

            let fetchedProduct = false;

            axios.get(url).then(response => {

                // set product
                fetchedProduct = true;
                setProduct(response.data);
                console.log(response.data);

            }).catch(error => {

                console.log("Error in fetching product details: ", error);

            })

            // create contract instance
            const drentContract = new ethers.Contract(drentContractAddress, drentContractAbi, provider);

            // check availability
            const availability = await drentContract.getProductAvailability(productHash);
            setIsAvailable(availability);

        }

    }, [provider]);

    // handle snackbar close
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnackbarOpen(false);
    };

    const handleStartDateChange = (date) => {
        setStartDate(date);
    }

    const handleEndDateChange = (date) => {
        setEndDate(date);
    }

    const handleCheckAVailability = () => {

        if (product) {

            // get end date from product (in seconds)
            const productEndDate = Number(product.lastDateAvailable) / 1000;
            console.log("Product End Date:", productEndDate);

            // calculate start date
            const productStartDate = productEndDate - Number(product.period - 1) * 24 * 3600;
            console.log("Product Start Date:", productStartDate);

            // current date
            const currentDate = ((new Date()).getTime()) / 1000;

            // check expiry of product
            if (currentDate >= productEndDate) {
                setAvailabilityMessage("Sorry, the product is already expired.");
                setIsValid(false);
                return;
            }

            // set start & end date (to start of selected date)
            setStartDate(new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate()));
            setEndDate(new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate()));

            // start & end date timestamp
            const startDateTimestamp = (new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate()).getTime()) / 1000;
            const endDateTimestamp = (new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate()).getTime()) / 1000;

            // console.log("End Date:", endDate.getTime(), endDateTimestamp);
            // console.log("Start Date:", startDate.getTime(), startDateTimestamp);

            // check if end date is after product end date
            if (endDateTimestamp >= productEndDate) {
                setAvailabilityMessage("Sorry, the product is not available in the given time range.");
                setIsValid(false);
                return;
            }

            // start index
            const startIndex = Math.ceil((Math.abs(startDateTimestamp - productStartDate)) / (24 * 3600));
            setStartIndex(startIndex);

            // end index
            const endIndex = startIndex + Math.ceil((Math.abs(endDateTimestamp - startDateTimestamp)) / (24 * 3600))
            setEndIndex(endIndex);

            let flag = true;
            for (let i = startIndex; i <= endIndex; i++) {
                if (isAvailable[i] == false) {
                    flag = false;
                    break;
                }
            }

            if (flag) {
                setAvailabilityMessage("The product is available in given time range.");
                setIsValid(true);
            } else {
                setAvailabilityMessage("Sorry, the product is not available in the given time range.");
                setIsValid(false);
            }

        }

        console.log(startDate);
        console.log(endDate);

    }

    const onProceed = () => {

        // check availability before proceed
        handleCheckAVailability();

        if (isValid) {

            // route to different path, with parameters
            history.push('/app/borrowproduct', {
                productHash: productHash,
                startDate: startDate,
                endDate: endDate,
                startIndex: startIndex,
                endIndex: endIndex
            });

        }

    }

    const AvailabilityForm = (
        <React.Fragment>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <div className={classes.sidediv} >
                    <KeyboardDatePicker
                        disableToolbar
                        variant="inline"
                        format="dd/MM/yyyy"
                        minDate={new Date()}
                        margin="normal"
                        id="start-date-picker-inline"
                        label="Select Start Date"
                        value={startDate}
                        onChange={handleStartDateChange}
                        KeyboardButtonProps={{
                            'aria-label': 'change date',
                        }}
                    />
                    <KeyboardDatePicker
                        disableToolbar
                        variant="inline"
                        format="dd/MM/yyyy"
                        minDate={startDate}
                        margin="normal"
                        id="end-date-picker-inline"
                        label="Select End Date"
                        value={endDate}
                        onChange={handleEndDateChange}
                        KeyboardButtonProps={{
                            'aria-label': 'change date',
                        }}
                    />
                </div>
            </MuiPickersUtilsProvider>
        </React.Fragment>
    )

    return (
        <div>

            <Header
                setParentAddress={(address) => { setAddress(address) }}
                setEthersProvider={(provider) => { setProvider(provider) }}
            />

            {address
                ?
                (product)
                    ?
                    <React.Fragment>
                        {/* <form noValidate autoComplete="off"> */}
                        <main className={classes.layout}>
                            <div className={classes.loadingbar}>
                                {loading ? <LinearProgress /> : null}
                            </div>

                            <Paper className={classes.paper}>

                                <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                                    <div>Borrowing Product, I See</div>
                                </Typography>

                                <div className={classes.updiv} style={{ justifyContent: 'space-between' }}>
                                    <div style={{ justifyContent: 'space-evenly' }} className={classes.sidediv}>

                                        {/* Image */}
                                        <div className={classes.updiv}>
                                            <img src={product.image[0]}></img>
                                        </div>

                                        {/* Product Details */}
                                        <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                                            <div className={classes.sidediv} style={{ justifyContent: 'space-between' }}>
                                                <div className={classes.updiv}>
                                                    <div>Owned By: </div>
                                                    <div>Name: </div>
                                                    <div>Description: </div>
                                                    <div>Price: </div>
                                                    <div>Collateral: </div>
                                                    {/* <div>Availability: </div> */}
                                                </div>
                                                <div className={classes.updiv}>
                                                    <div>
                                                        {product.owner}
                                                        <RouterLink to={`/app/profile/${product.owner}`}>
                                                            <LaunchIcon fontSize="small" />
                                                        </RouterLink>
                                                    </div>
                                                    <div>{product.name}</div>
                                                    <div>{product.description}</div>
                                                    <div>{product.price} DAI / day</div>
                                                    <div>{product.collateral} DAI</div>
                                                    {/* <div>Available</div> */}
                                                </div>
                                            </div>
                                        </Typography>


                                    </div>

                                </div>

                                <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                                    <div>Check Product Availability For Custom Date Range</div>
                                </Typography>

                                {AvailabilityForm}

                                <div>
                                    <Button
                                        type="submit"
                                        variant="contained"
                                        color="primary"
                                        className={classes.submit}
                                        onClick={handleCheckAVailability}
                                    >
                                        Check Availability
                                    </Button>
                                </div>

                                <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                                    <div>{availabilityMessage}</div>
                                </Typography>

                                <div className={classes.sidediv} style={{ gap: '24px' }}>
                                    <Typography component="h6" variant="h6" align="center">
                                        <div>Proceed to book product</div>
                                    </Typography>
                                    <Button
                                        type="submit"
                                        variant="contained"
                                        color="primary"
                                        disabled={!isValid}
                                        className={classes.submit}
                                        onClick={onProceed}
                                    >
                                        Proceed
                                    </Button>
                                </div>

                            </Paper>

                            <div className={classes.sidediv}>
                                <RouterLink to='/app'>
                                    <Button
                                        type="submit"
                                        variant="outlined"
                                        color="primary"
                                        className={classes.hspace}
                                    // onClick={(e) => { onProfileSave(e) }}
                                    >
                                        Back to Dashboard
                                    </Button>
                                </RouterLink>
                                <RouterLink to='/app/profile'>
                                    <Button
                                        type="submit"
                                        variant="outlined"
                                        color="primary"
                                        className={classes.hspace}
                                    // onClick={(e) => { onProfileSave(e) }}
                                    >
                                        View your profile
                                    </Button>
                                </RouterLink>
                                <RouterLink to='/app/addproducts'>
                                    <Button
                                        type="submit"
                                        variant="outlined"
                                        color="primary"
                                        className={classes.hspace}
                                    // onClick={(e) => { onProfileSave(e) }}
                                    >
                                        Add a product on rent
                                    </Button>
                                </RouterLink>
                            </div>
                        </main>
                        {/* </form> */}
                    </React.Fragment>
                    :
                    <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                        <div>
                            Oops...No product found!
                        </div>
                        <div className={classes.sidediv}>
                            <RouterLink to='/app'>
                                <Button
                                    type="submit"
                                    variant="outlined"
                                    color="primary"
                                    className={classes.hspace}
                                    to={'/app'}
                                // onClick={(e) => { onProfileSave(e) }}
                                >
                                    Back to Dashboard
                                </Button>
                            </RouterLink>
                        </div>
                    </Typography>
                :
                <Fragment>
                    <div className={classes.loadingbar}></div>
                    <main className={classes.layout}>
                        <Paper className={classes.paper}>
                            <Typography component="h1" variant="h4" align="center">
                                Please connect with a wallet to continue
                            </Typography>
                        </Paper>
                    </main>
                </Fragment>
            }

            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                open={snackbarOpen}
                autoHideDuration={4000}
                onClose={handleClose}
                message={snackbarMessage}
                action={
                    <React.Fragment>
                        <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                            <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment>
                }
            />

        </div>
    );

}

export default ViewProduct;