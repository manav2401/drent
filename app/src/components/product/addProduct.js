import React, { useState, useEffect, Fragment } from 'react';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import {
    TextField,
    Grid,
    AppBar,
    Button,
    Checkbox,
    IconButton,
    Paper,
    Toolbar,
    Typography,
    LinearProgress,
    Snackbar
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import MenuIcon from '@material-ui/icons/Menu';
import { ethers } from 'ethers';
import { Biconomy } from "@biconomy/mexa";
import axios from 'axios';

import Header from '../common/header';
import { useStyles } from '../common/styles';
import { drentContractAddress, drentContractAbi } from '../../artifacts/DrentContract';
import { pinIpfs } from '../../utils/pinIpfs';
import { UPDATE_REPUTATION } from '../../artifacts/uris';

const IpfsHttpClient = require('ipfs-http-client')

function AddProduct() {

    // use styles
    const classes = useStyles();

    // get history
    const history = useHistory();

    // user wallet address
    const [address, setAddress] = useState(null);
    const [provider, setProvider] = useState(null);
    const [contract, setContract] = useState(null);
    const [profileExists, setProfileExists] = useState(false);
    const [biconomy, setBiconomy] = useState(null);

    // ipfs client
    const [ipfsClient, setIpfsClient] = useState(null);

    // product
    const [name, setName] = useState(null);
    const [description, setDescription] = useState(null);
    const [period, setPeriod] = useState(null);
    const [price, setPrice] = useState(null);
    const [collateral, setCollateral] = useState(null);
    const [image, setImage] = useState([]);
    const [productHash, setProductHash] = useState(null);

    // other params
    const [loading, setLoading] = useState(false);
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState(null);

    useEffect(async () => {

        if (provider) {

            // set the ipfs client
            const ipfsClient = new IpfsHttpClient({
                url: new URL('https://ipfs.infura.io:5001')
            });

            setIpfsClient(ipfsClient);

            // get signer
            const signer = provider.getSigner();

            // create contract instance
            const drentContract = new ethers.Contract(drentContractAddress, drentContractAbi, signer);

            // check if profile exists
            try {

                const userProfile = await drentContract.getUser();
                setProfileExists(true);

                // instantiate biconomy
                const biconomy = new Biconomy(provider,
                    { apiKey: "MuxSIxgAM.902471e3-88ed-4d64-8d8b-8b81d368fc67" }
                );

                biconomy.onEvent(biconomy.READY, async () => {

                    console.log("Biconomy Initialized..");
                    setBiconomy(biconomy);

                }).onEvent(biconomy.ERROR, (error, message) => {
                    console.log("Error in biconomy initialization")
                    console.log("Error:", error, "Message", message);
                })

            } catch (error) {

                // profile doesn't exists
                console.log(error);
                setProfileExists(false);

            }

            // set contract in state
            setContract(drentContract);

        }


        // let ipfsUser = await fetch('https://ipfs.io/ipfs/QmbErZxVHQryaJ83fBT4iVs3H8KthC32LLt6JYyf1c7idf');
        // ipfsUser = await ipfsUser.json();
        // console.log("Data Returned: ", ipfsUser);

    }, [provider]);

    const handleNameChange = (e) => {
        setName(e.target.value);
    }

    const handleDescriptionChange = (e) => {
        setDescription(e.target.value);
    }

    const handlePeriodChange = (e) => {
        setPeriod(e.target.value);
    }

    const handlePriceChange = (e) => {
        setPrice(e.target.value);
    }

    const handleCollateralChange = (e) => {
        setCollateral(e.target.value);
    }

    const onProductSave = async (e) => {

        e.preventDefault();

        setLoading(true);

        // create json data
        let product = await createProductSchema();

        // get availability array
        let isAvailable = await getAvailabilityArray(product.period);

        // console.log("product:", product);
        // console.log("array:", isAvailable);

        try {
            // add product to ipfs
            const { cid } = await ipfsClient.add(JSON.stringify(product));

            if (cid.string) {

                setProductHash(cid.string);

                if (biconomy) {

                    // create contract instance with biconomy signer
                    let drentContract = new ethers.Contract(drentContractAddress, drentContractAbi, biconomy.getSignerByAddress(address));

                    // populate transaction
                    let { data } = await drentContract.populateTransaction.addProduct(cid.string, isAvailable);

                    // get biconomy provider
                    let biconomyProvider = biconomy.getEthersProvider();

                    let gasLimit = await provider.estimateGas({
                        to: drentContractAddress,
                        from: address,
                        data: data
                    });

                    let txParams = {
                        data: data,
                        to: drentContractAddress,
                        from: address,
                        gasLimit: gasLimit, // optional
                        signatureType: "EIP712_SIGN"
                    };

                    // as ethers does not allow providing custom options while sending transaction
                    let tx = await biconomyProvider.send("eth_sendTransaction", [txParams]);

                    console.log("tx:", tx);

                    //event emitter methods
                    provider.once(tx, async (transaction) => {
                        // Emitted when the transaction has been mined
                        //show success message
                        // console.log(transaction);
                        // pin hash
                        await pinIpfs(cid.string);

                        let body = {
                            "address": address,
                            "event": "rent"
                        }

                        const options = {
                            method: "POST",
                            url: UPDATE_REPUTATION,
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json;charset=UTF-8',
                            },
                            data: body
                        };

                        axios(options).then(response => {
                            console.log("Reputation updated");
                        }).catch(error => {
                            console.log("Error in updating reputation", error);
                        })

                        setSnackbarMessage("Product Added");
                        setSnackbarOpen(true);
                        setLoading(false);

                        // route to different path, with parameters
                        // history.push('/app');
                    });

                } else {

                    // biconomy not initialized
                    setSnackbarOpen(true);
                    setSnackbarMessage("Unable to add product. Try again later.");
                    setLoading(false);
                }

            } else {

                // unable to save to ipfs
                setSnackbarOpen(true);
                setSnackbarMessage("Unable to add product. Try again later.");
                setLoading(false);

            }
        } catch (error) {

            // metamask or ipfs error
            console.log("in catch:", error);
            setSnackbarOpen(true);
            setSnackbarMessage("Unable to add product. Try again later.");            
            setLoading(false);

        }

    }

    const createProductSchema = async () => {

        let promise = new Promise((resolve, reject) => {

            // get rent period start date
            const currentDate = new Date();
            const startDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());
            // console.log("Start Date:", startDate.toString());

            const endDate = new Date(startDate.valueOf());
            endDate.setDate(startDate.getDate() + (period - 1));
            // console.log("End Date:", endDate.toString());

            let product = {
                'owner': address,
                'name': name,
                'description': description,
                'period': period,
                'lastDateAvailable': endDate.valueOf(),
                'price': price,
                'collateral': collateral,
                'image': image,
            }

            resolve(product);

        })

        return promise;

    }

    const getAvailabilityArray = (period) => {

        let promise = new Promise((resolve, reject) => {

            const tempArray = [true];
            const isAvailable = Array.apply(null, { length: period }).map(() => { return tempArray[0] })
            resolve(isAvailable);

        })

        return promise;

    }

    const uploadImage = async (e) => {
        const file = e.target.files[0];
        try {
            const base64 = await convertBase64(file);
            // console.log('image uploaded: ' + base64);
            setImage([]);
            setImage([base64]);
        } catch (error) {
            console.log("Error in image upload");
            alert("Error in uploading image. Please try again.");
        }
    };

    const convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);

            fileReader.onload = () => {
                resolve(fileReader.result);
            };

            fileReader.onerror = (error) => {
                reject(error);
            };
        });
    };

    // handle snackbar close
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnackbarOpen(false);
    };

    const ProductForm = (
        <React.Fragment>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <TextField
                        required
                        id="name"
                        name="name"
                        label="Product Name"
                        fullWidth
                        value={name || ''}
                        onChange={handleNameChange}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        required
                        id="description"
                        name="description"
                        label="Product Description"
                        fullWidth
                        value={description || ''}
                        onChange={handleDescriptionChange}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        required
                        id="period"
                        name="period"
                        label="Number of days available (starting from today)"
                        fullWidth
                        value={period || ''}
                        onChange={handlePeriodChange}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        required
                        id="price"
                        name="price"
                        label="Per day rent price (valid upto 4 decimal places)"
                        fullWidth
                        value={price || ''}
                        onChange={handlePriceChange}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        required
                        id="collateral"
                        name="collateral"
                        label="Collateral Amount (valid upto 4 decimal places)"
                        fullWidth
                        value={collateral || ''}
                        onChange={handleCollateralChange}
                    />
                </Grid>
                {/* <Grid item xs={12}>
                    <TextField
                        required
                        id="image"
                        name="image"
                        label="Image"
                        fullWidth
                        value={image || ''}
                    // onChange={handleCityChange}
                    />
                </Grid> */}
                <Grid item xs={12}>
                    <div className={classes.sidediv}>
                        <div className={classes.hspace}>
                            Choose an image of product
                        </div>
                        <input className={classes.hspace}
                            type="file"
                            onChange={(e) => { uploadImage(e); }}
                        />
                    </div>
                </Grid>
                <Button
                    className={classes.vspace}
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    // className={classes.submit}
                    onClick={(e) => { onProductSave(e) }}
                >
                    Save
                </Button>
            </Grid>
        </React.Fragment>
    )

    return (
        <div>

            <Header
                setParentAddress={(address) => { setAddress(address) }}
                setEthersProvider={(provider) => { setProvider(provider) }}
            />

            {address
                ?
                (profileExists)
                    ?
                    <React.Fragment>
                        <form noValidate autoComplete="off">
                            <main className={classes.layout}>
                                <div className={classes.loadingbar}>
                                    {loading ? <LinearProgress /> : null}
                                </div>
                                <Paper className={classes.paper}>
                                    <Typography component="h1" variant="h4" align="center">
                                        <div>
                                            {address ? ('Hello ' + address) : ''}
                                        </div>
                                    </Typography>
                                    <React.Fragment>
                                        {ProductForm}
                                    </React.Fragment>
                                </Paper>
                                <div className={classes.sidediv}>
                                    <RouterLink to='/app'>
                                        <Button
                                            type="submit"
                                            variant="outlined"
                                            color="primary"
                                            className={classes.hspace}
                                            to={'/app'}
                                        // onClick={(e) => { onProfileSave(e) }}
                                        >
                                            Go to Dashboard
                                    </Button>
                                    </RouterLink>
                                    <RouterLink to='/app/profile'>
                                        <Button
                                            type="submit"
                                            variant="outlined"
                                            color="primary"
                                            className={classes.hspace}
                                        // onClick={(e) => { onProfileSave(e) }}
                                        >
                                            View your profile
                                    </Button>
                                    </RouterLink>

                                </div>
                            </main>
                        </form>
                    </React.Fragment>
                    :
                    <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                        <div>
                            Looks like you haven't completed your profile...Profile Completion is mandatory to add product!
                        </div>
                    </Typography>
                :
                <Fragment>
                    <div className={classes.loadingbar}></div>
                    <main className={classes.layout}>
                        <Paper className={classes.paper}>
                            <Typography component="h1" variant="h4" align="center">
                                Please connect with a wallet to continue
                            </Typography>
                        </Paper>
                    </main>
                </Fragment>
            }

            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                open={snackbarOpen}
                autoHideDuration={4000}
                onClose={handleClose}
                message={snackbarMessage}
                action={
                    <React.Fragment>
                        <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                            <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment>
                }
            />

        </div>
    );

}

export default AddProduct;