import React, { useState, useEffect, Fragment } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import {
    TextField,
    Grid,
    AppBar,
    Button,
    Checkbox,
    IconButton,
    Paper,
    Toolbar,
    Typography,
    LinearProgress,
    Snackbar
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import MenuIcon from '@material-ui/icons/Menu';
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
} from '@material-ui/pickers';
// import MomentUtils from '@date-io/moment';
import DateFnsUtils from '@date-io/date-fns';
import 'date-fns';

import { ethers } from 'ethers';
import axios from 'axios';

import Header from '../common/header';
import { useStyles } from '../common/styles';
import { drentContractAddress, drentContractAbi } from '../../artifacts/DrentContract';
import { daiContractAddress, daiContractAbi } from '../../artifacts/DaiContract';
import { pinIpfs } from '../../utils/pinIpfs';
import { UPDATE_REPUTATION } from '../../artifacts/uris';

const IpfsHttpClient = require('ipfs-http-client')

function BorrowProduct(props) {

    const classes = useStyles();

    // user wallet address
    const [address, setAddress] = useState(null);
    const [provider, setProvider] = useState(null);
    const [contract, setContract] = useState(null);
    const [profileExists, setProfileExists] = useState(false);

    // ipfs client
    const [ipfsClient, setIpfsClient] = useState(null);

    // product details
    const [product, setProduct] = useState(null);
    const [productHash, setProductHash] = useState(null);
    const [isAvailable, setIsAvailable] = useState(null);
    const [startDate, setStartDate] = useState(null);
    const [endDate, setEndDate] = useState(null);
    const [numberOfDays, setNumberOfDays] = useState(null);
    const [startIndex, setStartIndex] = useState(null);
    const [endIndex, setEndIndex] = useState(null);

    // other params
    const [errorMessage, setErrorMessage] = useState(null);
    const [loading, setLoading] = useState(false);
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState(null);

    useEffect(async () => {

        if (provider && props.location.state) {

            // set the ipfs client
            const ipfsClient = new IpfsHttpClient({
                url: new URL('https://ipfs.infura.io:5001')
            });

            setIpfsClient(ipfsClient);

            // get signer
            const signer = provider.getSigner();

            // create contract instance
            const drentContract = new ethers.Contract(drentContractAddress, drentContractAbi, signer);

            // set contract in state
            setContract(drentContract);

            // check if profile exists
            let userProfile = null;
            try {

                userProfile = await drentContract.getUser();
                setProfileExists(true);

            } catch (error) {

                // profile doesn't exists
                console.log(error);
                setProfileExists(false);

            }

            // fetch product availability
            let availability = null;
            try {
                availability = await drentContract.getProductAvailability(props.location.state.productHash);
                setIsAvailable(availability);
            } catch (error) {
                console.log(error);
            }

            if (userProfile) {

                if (availability) {

                    setProductHash(props.location.state.productHash);
                    setStartDate(props.location.state.startDate);
                    setEndDate(props.location.state.endDate);
                    setStartIndex(props.location.state.startIndex);
                    setEndIndex(props.location.state.endIndex);

                    // start & end date timestamp
                    const startDateTimestamp = (new Date(props.location.state.startDate).getTime()) / 1000;
                    const endDateTimestamp = (new Date(props.location.state.endDate).getTime()) / 1000;

                    // calculate number of days
                    const days = Math.ceil((Math.abs(endDateTimestamp - startDateTimestamp)) / (24 * 3600)) + 1;

                    setNumberOfDays(days);

                    const url = "https://ipfs.io/ipfs/" + props.location.state.productHash;

                    axios.get(url).then(async (response) => {

                        // set product
                        setProduct(response.data);

                    }).catch(error => {

                        console.log("Error in fetching product details: ", error);

                    })

                } else {
                    // availability not fetched
                }

            }

        } else {
            // no profile found
        }

    }, [provider]);

    // handle snackbar close
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnackbarOpen(false);
    };

    const makePayment = async () => {

        setLoading(true);

        // start & end date timestamp
        const startDateTimestamp = (new Date(startDate).getTime()) / 1000;
        const endDateTimestamp = (new Date(endDate).getTime()) / 1000;

        // calculate total amount
        let totalAmount = Number(numberOfDays * product.price) + Number(product.collateral);
        totalAmount = totalAmount * 10000;

        // update availability array
        let availability = [...isAvailable];
        for (let i = startIndex; i <= endIndex; i++) {
            availability[i] = false;
        }

        // console.log(availability);

        // create order json here
        const order = {
            ownerAddress: product.owner,
            borrowerAddress: address,
            productId: productHash,
            startDate: startDateTimestamp,
            endDate: endDateTimestamp,
            disputeCreated: false,
            rentAmount: Number(numberOfDays * product.price),
            collateralAmount: Number(product.collateral)
        }

        // send data to ipfs
        try {

            const { cid } = await ipfsClient.add(JSON.stringify(order));

            console.log("IPFS:", cid.string);

            if (cid.string) {

                // get signer
                const signer = provider.getSigner();

                // create dai contract instance
                const daiContract = new ethers.Contract(daiContractAddress, daiContractAbi, signer);

                let approveResponse = null;
                let placeOrderResponse = null;
                let approveReceipt = null;
                let placeOrderReceipt = null;
                try {

                    // call the drent contract to transfer funds
                    approveResponse = await daiContract.approve(drentContractAddress, totalAmount);

                    // wait for confirmation
                    approveReceipt = await approveResponse.wait();

                    let overrides = {
                        value: totalAmount.toString()
                    };
                    placeOrderResponse = await contract.placeOrder(cid.string, product.owner, productHash, availability, overrides);

                    // wait for confirmation
                    placeOrderReceipt = await placeOrderResponse.wait();

                } catch (error) {
                    console.log("Error in payment:", error);
                }

                if (placeOrderReceipt) {
                    console.log("Place Order Response:", placeOrderReceipt);
                    await pinIpfs(cid.string);
                    
                    let body = {
                        "address": address,
                        "event": "borrow"
                    }

                    const options = {
                        method: "POST",
                        url: UPDATE_REPUTATION,
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json;charset=UTF-8',
                        },
                        data: body
                    };

                    axios(options).then(response => {
                        console.log("Reputation updated");
                    }).catch(error => {
                        console.log("Error in updating reputation", error);
                    })

                    setSnackbarMessage("Order added");
                    // setErrorMessage(null);
                } else {
                    console.log("Error in adding order.");
                    // setErrorMessage("");
                    setSnackbarMessage("Unable to save order. Try again later.");
                }

            } else {

                console.log("Error in adding ipfs...");
                // setErrorMessage("Unable to place order. Try again later.");
                setSnackbarMessage("Unable to save order. Try again later.");

            }

            setLoading(false);
            setSnackbarOpen(true);


        } catch (error) {

            console.log("Error in adding order.", error);
            // setErrorMessage("Unable to place order. Try again later.");
            setSnackbarMessage("Unable to save order. Try again later.");
            setSnackbarOpen(true);
            setLoading(false);

        }

    }

    return (
        <div>

            <Header
                setParentAddress={(address) => { setAddress(address) }}
                setEthersProvider={(provider) => { setProvider(provider) }}
            />

            {address
                ?
                profileExists
                    ?
                    (product)
                        ?
                        <React.Fragment>
                            {/* <form noValidate autoComplete="off"> */}
                            <main className={classes.layout}>
                                <div className={classes.loadingbar}>
                                    {loading ? <LinearProgress /> : null}
                                </div>

                                <Paper className={classes.paper}>

                                    <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                                        <div>Review your order</div>
                                    </Typography>

                                    <div className={classes.updiv} style={{ justifyContent: 'space-between' }}>
                                        <div style={{ justifyContent: 'space-evenly' }} className={classes.sidediv}>

                                            {/* Image */}
                                            <div className={classes.updiv}>
                                                <img src={product.image[0]}></img>
                                            </div>

                                            {/* Product Details */}
                                            <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                                                <div className={classes.sidediv} style={{ justifyContent: 'space-between' }}>
                                                    <div className={classes.updiv}>
                                                        <div>Name: </div>
                                                        <div>Description: </div>
                                                        <div>Rent Price: </div>
                                                        <div>Collateral: </div>
                                                        <div>Start Date: </div>
                                                        <div>End Date: </div>
                                                        <div>Total Price Payable: </div>
                                                    </div>
                                                    <div className={classes.updiv}>
                                                        <div>{product.name}</div>
                                                        <div>{product.description}</div>
                                                        <div>{product.price} DAI / day</div>
                                                        <div>{product.collateral} DAI</div>
                                                        <div>{startDate.getDate()}/{startDate.getMonth() + 1}/{startDate.getFullYear()}</div>
                                                        <div>{endDate.getDate()}/{endDate.getMonth() + 1}/{endDate.getFullYear()}</div>
                                                        <div>{Number(numberOfDays * product.price) + Number(product.collateral)} DAI</div>
                                                    </div>
                                                </div>
                                            </Typography>
                                        </div>
                                    </div>

                                    <div className={classes.sidediv} style={{ gap: '24px' }}>
                                        <Typography component="h6" variant="h6" align="center">
                                            <div>Proceed to pay</div>
                                        </Typography>
                                        <Button
                                            type="submit"
                                            variant="contained"
                                            color="primary"
                                            className={classes.submit}
                                            onClick={makePayment}
                                        >
                                            Pay {Number(numberOfDays * product.price) + Number(product.collateral)} DAI
                                    </Button>
                                    </div>

                                    {/* <Typography component="h6" variant="h6" align="center">
                                        <div>{errorMessage}</div>
                                    </Typography> */}

                                </Paper>

                                <div className={classes.sidediv}>
                                    <RouterLink to={`/app/products/${productHash}`}>
                                        <Button
                                            type="submit"
                                            variant="outlined"
                                            color="primary"
                                            className={classes.hspace}
                                        // onClick={(e) => { onProfileSave(e) }}
                                        >
                                            Change Date
                                    </Button>
                                    </RouterLink>
                                    <RouterLink to='/app/orders'>
                                        <Button
                                            type="submit"
                                            variant="outlined"
                                            color="primary"
                                            className={classes.hspace}
                                        // onClick={(e) => { onProfileSave(e) }}
                                        >
                                            View Orders
                                    </Button>
                                    </RouterLink>
                                    <RouterLink to='/app'>
                                        <Button
                                            type="submit"
                                            variant="outlined"
                                            color="primary"
                                            className={classes.hspace}
                                        // onClick={(e) => { onProfileSave(e) }}
                                        >
                                            Back to Dashboard
                                    </Button>
                                    </RouterLink>
                                </div>
                            </main>
                            {/* </form> */}
                        </React.Fragment>
                        :
                        <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                            <div>
                                Oops...Looks like you're lost.
                        </div>
                            <div className={classes.sidediv}>
                                <RouterLink to='/app'>
                                    <Button
                                        type="submit"
                                        variant="outlined"
                                        color="primary"
                                        className={classes.hspace}
                                        to={'/app'}
                                    // onClick={(e) => { onProfileSave(e) }}
                                    >
                                        Back to Dashboard
                                </Button>
                                </RouterLink>
                            </div>
                        </Typography>
                    :
                    <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                        <div>
                            Looks like you haven't completed your profile...Profile Completion is mandatory to place order!
                        </div>
                    </Typography>
                :
                <Fragment>
                    <div className={classes.loadingbar}></div>
                    <main className={classes.layout}>
                        <Paper className={classes.paper}>
                            <Typography component="h1" variant="h4" align="center">
                                Please connect with a wallet to continue
                            </Typography>
                        </Paper>
                    </main>
                </Fragment>
            }

            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                open={snackbarOpen}
                autoHideDuration={4000}
                onClose={handleClose}
                message={snackbarMessage}
                action={
                    <React.Fragment>
                        <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                            <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment>
                }
            />

        </div>
    );

}

export default BorrowProduct;