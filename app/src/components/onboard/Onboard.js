import { ethers } from 'ethers';
import { useState, useEffect } from 'react';
import InitOnboard from './InitOnboard';

var provider;

function OnBoard() {

    const [address, setAddress] = useState(null);
    const [wallet, setWallet] = useState(null);
    const [onboard, setOnboard] = useState(null);

    useEffect(() => {
        const onboard = InitOnboard({
            wallet: wallet => {
                setWallet(wallet);
                const ethersProvider = new ethers.providers.Web3Provider(
                    wallet.provider
                );
                provider = ethersProvider;
                window.localStorage.setItem('selectedWallet', wallet.name);
            },
            address: address => {
                setAddress(address);
                if (!address) {
                    window.localStorage.removeItem('selectedWallet');
                    window.localStorage.removeItem('address');
                } else {
                    window.localStorage.setItem('address', address);
                }
            }

        });
        setOnboard(onboard);
    }, [])

    useEffect(() => {
        const selectedWallet = window.localStorage.getItem('selectedWallet');
        if (selectedWallet && onboard) {
            onboard.walletSelect(selectedWallet);
        }
    }, [onboard])

    async function connectToWallet() {
        if (onboard) {
            await onboard.walletSelect();
            await onboard.walletCheck();
        }
    }

    return (
        <div>
            <h2>Hello world.</h2>
            {address ? <span>address: {address}</span> : <button onClick={connectToWallet}>Connect to wallet</button>}

        </div>
    )

}

export default OnBoard;