import Onboard from 'bnc-onboard';
import { ganache } from '../../artifacts/networkIds';

function InitOnboard(subscriptions) {

    const onboard = Onboard({
        networkId: ganache,
        networkName: "Ganache",
        subscriptions,
        walletSelect: {
            wallets: [
                { walletName: 'metamask' }
            ]
        },
        walletCheck: [
            { checkName: 'connect' },
            { checkName: 'network' }
        ]
    });

    return onboard;

}

export default InitOnboard;