import { useEffect, useState } from "react"

import { ethers } from 'ethers'
import { Button } from "@material-ui/core";

import { forwardAddress, forwardAbi } from '../../artifacts/biconomyForwardTests';
import { daiContractAddress } from '../../artifacts/DaiContract';

import { Biconomy } from "@biconomy/mexa";

export default function ERC20Test() {

    const [provider, setProvider] = useState(null);
    const [userAddress, setUserAddress] = useState(null);

    useEffect(async () => {

        // testing biconomy forward

        if (window.ethereum) {

            window.ethereum.enable();

            let ethersProvider = new ethers.providers.Web3Provider(window.ethereum);
            let addr = await ethersProvider.listAccounts();
            addr = addr[0];

            console.log("user address:", addr);

            setProvider(ethersProvider);
            setUserAddress(addr);

        }

    }, []);

    const handleClick = async () => {

        // initialize biconomy
        const biconomy = new Biconomy(provider,
            { apiKey: "FG87LB-1d.3e744cc6-dc81-4e69-9087-1fed5c9e250b", debug: true }
        );

        // wait for event
        biconomy.onEvent(biconomy.READY, async () => {

            console.log("Biconomy Initialized...ready to use");

            // create ethers provider from biconomy object
            let ethersProvider = new ethers.providers.Web3Provider(biconomy);

            // get signer
            let signer = ethersProvider.getSigner();

            console.log("erc20forwarderclient:", biconomy.erc20ForwarderClient);
            console.log("permit client:", biconomy.permitClient);

            // get erc20 forwarder client
            let ercForwarderClient = biconomy.erc20ForwarderClient;

            // get permit client
            let permitClient = biconomy.permitClient;

            // create new contract instance
            let contract = new ethers.Contract(forwardAddress, forwardAbi, signer.connectUnchecked());

            // create interface
            let contractInterface = new ethers.utils.Interface(forwardAbi);

            // default spender: Biconomy's ERC20 Fee Proxy Contract
            const daiPermitOptions = {
                expiry: Math.floor(Date.now() / 1000 + 3600),
                allowed: true
            }

            // getting permit
            console.log("getting permit to spend tokens");
            let permitTx = await permitClient.daiPermit(daiPermitOptions);
            await permitTx.wait(1);

            // populate transaction
            let functionSignature = contractInterface.encodeFunctionData("set", ["Manav Darji", { value: "20000" }]);

            // Create your target method signature.. here we are calling addRating() method of our contract
            // let functionSignature = contract.encodeFunctionData("functionName", ["hello meta transactions"]);
            let gasPrice = await ethersProvider.getGasPrice();
            let gasLimit = await ethersProvider.estimateGas({
                to: forwardAddress,
                from: userAddress,
                data: functionSignature,
            });
            console.log(gasLimit.toString());
            console.log(gasPrice.toString());
            // console.log(data);

            const builtTx = await ercForwarderClient.buildTx({
                to: forwardAddress,
                token: daiContractAddress,
                txGas: Number(gasLimit),
                data: functionSignature
            });
            const tx = builtTx.request;
            console.log("tx:", tx);
            //Show the fee to your users!!!)
            const fee = builtTx.cost;
            // number of ERC20 tokens user will pay on behalf of gas for this transaction
            console.log("number of ERC20 tokens user will pay on behalf of gas for this transaction")
            console.log(fee);

            // returns a json object with txHash (if transaction is successful), log, message, code and flag
            const transaction = await ercForwarderClient.sendTxEIP712Sign({ req: tx });
            // const txHash = txResponse.txHash;
            console.log(transaction);

            if (transaction && transaction.code == 200 && transaction.txHash) {
                //event emitter methods
                ethersProvider.once(transaction.txHash, (result) => {
                    // Emitted when the transaction has been mined
                    console.log(result);
                });
            }

        }).onEvent(biconomy.ERROR, (error, message) => {
            console.log("Error:", error, "Message", message);
        })

    }

    return (
        <div>
            <p>Hello</p>
            <Button onClick={handleClick}>
                Click Me
            </Button>
        </div>
    )
}