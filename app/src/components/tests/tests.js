import { useEffect, useState } from "react"

import { ethers } from 'ethers'
import { Button } from "@material-ui/core";

import { abi, address } from '../../artifacts/testContracts';

import { drentContractAbi, drentContractAddress } from '../../artifacts/DrentContract';
import { ownableTestAbi, ownableTestAddress } from '../../artifacts/ownableTest';

import { Biconomy } from "@biconomy/mexa";

export default function Test() {

    const [biconomy, setBiconomy] = useState(null);
    const [userAddress, setUserAddress] = useState(null);

    const [image, setImage] = useState(null);

    useEffect(async () => {


        // if (window.ethereum) {

        //     window.ethereum.enable();

        //     // let provider = new ethers.providers.Web3Provider(window.ethereum);

        //     let provider = new ethers.providers.getDefaultProvider();
        //     let wallet = new ethers.Wallet("8ab8e1e4e5004999b5933a5324f07d2b0bd1c9ad058be4b707ab6d9cf88ce7ed", provider);

        //     // let signer = provider.getSigner();

        //     let contract = new ethers.Contract(ownableTestAddress, ownableTestAbi, wallet);

        //     let overrides = {
        //         from: "0xA7aFEe592B42Fb12a4D0913cF172A6a32Aa00C46"
        //     }

        //     let tx = await contract.setTest(0, "World", overrides);

        //     let receipt = await tx.wait();

        //     console.log("receipt:", receipt);

        //     // try {

        //     //     let wallet = new ethers.Wallet("8ab8e1e4e5004999b5933a5324f07d2b0bd1c9ad058be4b707ab6d9cf88ce7ed");
        //     //     let provider = ethers.getDefaultProvider();

        //     //     let contract = new ethers.Contract(drentContractAddress, drentContractAbi, provider);

        //     //     let { data } = await contract.populateTransaction.getAllProducts();

        //     //     let gasLimit = await provider.estimateGas({
        //     //         to: drentContractAddress,
        //     //         data: data
        //     //     });

        //     //     let params = {
        //     //         data: data,
        //     //         to: drentContractAddress,
        //     //         gasLimit: gasLimit
        //     //     }

        //     //     let signature = await wallet.signTransaction(params);

        //     //     let resp = await provider.sendTransaction(signature);

        //     //     console.log("resp:", resp.data);

        //     // } catch (error) {
        //     //     console.log("hello:", error);
        //     // }

        // }


    }, []);



    const handleClickWallet = async () => {

        // let web3Provider = new ethers.providers.Web3Provider(window.ethereum);
        // console.log("web3:", web3Provider);

        // let web3provider = new Web3.providers.HttpProvider("https://rpc-mumbai.maticvigil.com");

        // let wallet = new ethers.Wallet("8ab8e1e4e5004999b5933a5324f07d2b0bd1c9ad058be4b707ab6d9cf88ce7ed", ethersProvider);

        // console.log("wallet provider:", wallet.provider == ethersProvider);
        // const biconomy = new Biconomy(web3provider,
        //     { apiKey: "1TBUlj0L8.c83c71bf-d539-4d84-9a9d-4584ba6c19b0", debug: true }
        // );
        // // ethersProvider = new ethers.providers.BaseProvider(biconomy);

        // biconomy.onEvent(biconomy.READY, async () => {

        //     console.log("Biconomy Initialized..");
        //     // console.log(ethersProvider);

        // }).onEvent(biconomy.ERROR, (error, message) => {
        //     console.log("Error in biconomy initialization")
        //     console.log("Error:", error, "Message", message);
        // })

    }

    const handleClick = async () => {

        if (biconomy) {

            // Initialize Constants
            console.log("Before contract creation...")
            let contract = new ethers.Contract(address, abi, biconomy.getSignerByAddress(userAddress));
            console.log("After contract creation...")

            console.log("Before populate transaction..")
            let { data } = await contract.populateTransaction.set("Biconomy Test 4");
            console.log("After populate transaction..")

            console.log("Before get ethers provider..")
            let provider = biconomy.getEthersProvider();
            console.log("After get ethers provider..")

            console.log("Before estimate gas..")
            let gasLimit = await provider.estimateGas({
                to: address,
                from: userAddress,
                data: data
            });
            console.log("After estimate gas...Limit:", gasLimit)

            let txParams = {
                data: data,
                to: address,
                from: userAddress,
                gasLimit: gasLimit, // optional
                // signatureType: "EIP712_SIGN"
            };

            console.log("Before send transaction...");
            // as ethers does not allow providing custom options while sending transaction                 
            let tx = await provider.send("eth_sendTransaction", [txParams]);
            console.log("After send transaction...");
            console.log("Transaction hash : ", tx);

            //event emitter methods
            provider.once(tx, (transaction) => {
                // Emitted when the transaction has been mined
                //show success message
                console.log(transaction);
                //do something with transaction hash
            });
        }

    }

    const uploadImage = async (e) => {
        const file = e.target.files[0];
        try {
            const base64 = await convertBase64(file);
            // console.log('image uploaded: ' + base64);
            setImage();
            setImage(base64);
        } catch (error) {
            console.log("Error in image upload");
            alert("Error in uploading image. Please try again.");
        }
    };

    const convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);

            fileReader.onload = () => {
                resolve(fileReader.result);
            };

            fileReader.onerror = (error) => {
                reject(error);
            };
        });
    };

    return (
        <div>
            <p>Hello</p>
            <input
                type="file"
                onChange={(e) => { uploadImage(e); }}
            />
            <img src={image}></img>
        </div >
    )

}