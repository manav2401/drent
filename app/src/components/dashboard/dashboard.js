import React, { useState, useEffect, Fragment } from 'react';
import { Link, Link as RouterLink } from 'react-router-dom';
import {
    TextField,
    Grid,
    Button,
    IconButton,
    Paper,
    Typography,
    Snackbar,
    Card,
    CardContent,
    CardMedia,
    Container,
    Modal
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import MenuIcon from '@material-ui/icons/Menu';
import { ethers } from 'ethers';
import axios from 'axios';

import Header from '../common/header';
import { useStyles } from '../common/styles';
import { drentContractAddress, drentContractAbi } from '../../artifacts/DrentContract';

const IpfsHttpClient = require('ipfs-http-client')

function getModalStyle() {
    return {
        top: '50%',
        left: '50%',
        transform: `translate(-50%, -50%)`,
    };
}

function Dashboard() {

    // use styles
    const classes = useStyles();
    const [modalStyle] = React.useState(getModalStyle);

    // user wallet address
    const [address, setAddress] = useState(null);
    const [provider, setProvider] = useState(null);
    const [contract, setContract] = useState(null);

    // ipfs client
    const [ipfsClient, setIpfsClient] = useState(null);

    // products
    const [products, setProducts] = useState([]);

    // other params
    const [loading, setLoading] = useState(false);
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState(null);
    const [modalOpen, setModalOpen] = useState(false);
    const [selectedProductIndex, setSelectedProductIndex] = useState(null);

    useEffect(async () => {

        if (provider) {

            const ipfsClient = new IpfsHttpClient({
                url: new URL('https://ipfs.infura.io:5001')
            });

            setIpfsClient(ipfsClient);

            // get signer
            // const signer = provider.getSigner(0);

            // create contract instance
            const drentContract = new ethers.Contract(drentContractAddress, drentContractAbi, provider);
            setContract(drentContract);

            let productsHash = null;
            try {
                productsHash = await drentContract.getAllProducts();
            } catch (error) {
                console.log("Unable to fetch products", error);
            }

            if (productsHash) {

                let productObject = []// new Array(productsHash.length);
                let url = "";

                for (let i = 0; i < productsHash.length; i++) {

                    url = "https://ipfs.io/ipfs/" + productsHash[i].toString();

                    axios.get(url).then(response => {

                        let resp = response.data;
                        resp.hash = productsHash[i].toString();

                        productObject.push(resp);

                        if (productObject.length == productsHash.length) {
                            setProducts(productObject);
                        }

                    }).catch(error => {
                        console.log(error);
                    });

                }

            }

        }

    }, [provider]);

    const handleProductClick = (index) => {
        setModalOpen(true);

        // fetch product owner's location here

        setSelectedProductIndex(index);
    }

    const handleModalClose = () => {
        setModalOpen(false);
        setSelectedProductIndex(null);
    }

    // handle snackbar close
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnackbarOpen(false);
    };

    return (
        <div>

            <Header
                setParentAddress={(address) => { setAddress(address) }}
                setEthersProvider={(provider) => { setProvider(provider) }}
            />

            {address
                ?
                <React.Fragment>
                    <main>
                        {/* Top Message */}
                        <Paper className={classes.paper}>
                            <Typography component="h1" variant="h4" align="center">
                                <div>
                                    A decentralized C2C renting marketplace
                                </div>
                            </Typography>
                        </Paper>

                        {/* Cards */}
                        {(products.length != 0)
                            ?
                            <div className={classes.vspace}>
                                <Container maxWidth="md">
                                    <Grid container spacing={4}>
                                        {products.map((product, i) => (
                                            { product }
                                                ?
                                                <Grid item key={product.hash} xs={12} sm={6} md={4}>
                                                    <div>
                                                        <Card className={classes.card}>
                                                            <CardMedia
                                                                className={classes.cardMedia}
                                                                image={product.image}
                                                                title={product.name}
                                                            />
                                                            <CardContent className={classes.cardContent}>
                                                                <Typography gutterBottom variant="h5" component="h2">
                                                                    {product.name}
                                                                </Typography>
                                                                <Typography>
                                                                    {product.price} DAI / day
                                                                </Typography>
                                                            </CardContent>
                                                            <Link to={`/app/products/${product.hash}`}>
                                                                <Button variant="contained" color="primary">
                                                                    Show More
                                                                </Button>
                                                            </Link>

                                                            {/* {(selectedProductIndex != null)
                                                                ?
                                                                <Modal
                                                                    open={modalOpen}
                                                                    onClose={handleModalClose}
                                                                    style={{ overlay: { zIndex: 1000 } }}
                                                                >
                                                                    <div style={modalStyle} className={classes.modal}>
                                                                        <img src={products[selectedProductIndex].image}>
                                                                        </img>
                                                                        <h2 id="simple-modal-title">{products[selectedProductIndex].name}</h2>
                                                                        {(products[selectedProductIndex].description != (null || undefined))
                                                                            ?
                                                                            <p id="simple-modal-description">
                                                                                Description: {products[selectedProductIndex].description}
                                                                            </p>
                                                                            :
                                                                            <div></div>
                                                                        }
                                                                        {(products[selectedProductIndex].collateral != (null || undefined))
                                                                            ?
                                                                            <p id="simple-modal-description">
                                                                                Collateral: {products[selectedProductIndex].collateral}$
                                                                            </p>
                                                                            :
                                                                            <div></div>
                                                                        }
                                                                        {(products[selectedProductIndex].price != (null || undefined))
                                                                            ?
                                                                            <p id="simple-modal-description">
                                                                                Price: {products[selectedProductIndex].price}$ / day
                                                                            </p>
                                                                            :
                                                                            <div></div>
                                                                        }
                                                                        <RouterLink to={{
                                                                            pathname: "/app/products/borrow",
                                                                            productHash: products[selectedProductIndex].hash
                                                                        }}>
                                                                            <Button
                                                                                type="submit"
                                                                                variant="contained"
                                                                                color="primary"
                                                                                className={classes.button}
                                                                            >
                                                                                Check Availability
                                                                            </Button>
                                                                        </RouterLink>

                                                                    </div>
                                                                </Modal>
                                                                :
                                                                <div></div>
                                                            } */}

                                                        </Card>
                                                    </div>
                                                    {/* <Button onClick={() => handleClick(product.hash, i)}>
                                                        Click
                                                    </Button> */}
                                                </Grid>
                                                :
                                                <div></div>
                                        ))}

                                    </Grid>
                                </Container>
                            </div>
                            :
                            <Typography component="h6" variant="h6" align="center">
                                <div>
                                    No products to display...Why not try adding one?
                                </div>
                            </Typography>
                        }

                    </main>
                </React.Fragment>
                :
                <Fragment>
                    <div className={classes.loadingbar}></div>
                    <main className={classes.layout}>
                        <Paper className={classes.paper}>
                            <Typography component="h1" variant="h4" align="center">
                                Please connect with a wallet to continue
                            </Typography>
                        </Paper>
                    </main>
                </Fragment>
            }

            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                open={snackbarOpen}
                autoHideDuration={4000}
                onClose={handleClose}
                message={snackbarMessage}
                action={
                    <React.Fragment>
                        <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                            <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment>
                }
            />

        </div>
    );

}

export default Dashboard;