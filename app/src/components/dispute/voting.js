import React, { useState, useEffect, Fragment } from 'react';
import { Link, Link as RouterLink } from 'react-router-dom';
import {
    TextField,
    Grid,
    Button,
    IconButton,
    Paper,
    Typography,
    Snackbar,
    Card,
    CardContent,
    CardMedia,
    Container,
    Modal
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import MenuIcon from '@material-ui/icons/Menu';
import { ethers } from 'ethers';
import axios from 'axios';
import socketIOClient from "socket.io-client";
import Header from '../common/header';
import { useStyles } from '../common/styles';
import { drentContractAddress, drentContractAbi } from '../../artifacts/DrentContract';

const IpfsHttpClient = require('ipfs-http-client')

function getModalStyle() {
    return {
        top: '50%',
        left: '50%',
        transform: `translate(-50%, -50%)`,
    };
}

function Voting() {

    const socket = socketIOClient("http://localhost:5000/");

    // use styles
    const classes = useStyles();
    const [modalStyle] = React.useState(getModalStyle);

    // user wallet address
    const [address, setAddress] = useState(null);
    const [provider, setProvider] = useState(null);
    const [contract, setContract] = useState(null);
    const [msg, setMsg] = useState(null);

    // ipfs client
    const [ipfsClient, setIpfsClient] = useState(null);

    // products
    const [disputes, setDisputes] = useState([]);

    const dis = {
        ownerSide: 0,
        borrowerSide: 0,
        description: "Hey",
        _id: "6063729923cbf6407556f2cf"
    }

    useEffect(async () => {

        if (provider) {

            const ipfsClient = new IpfsHttpClient({
                url: new URL('https://ipfs.infura.io:5001')
            });

            setIpfsClient(ipfsClient);

            // get signer
            const signer = provider.getSigner();

            // create contract instance
            const drentContract = new ethers.Contract(drentContractAddress, drentContractAbi, signer);
            setContract(drentContract);
            
            socket.emit("allVotes");
            socket.on("get_data", setData);
            socket.on("change_vote", setNewData);
        }

    }, [provider]);

    const setNewData = (updatedDispute) => {
        let arr = [...disputes];
        arr[0] = updatedDispute;
        setDisputes(arr);
    }

    const setData = (data) => {
        console.log("Heyy");
        console.log(data);
        setDisputes(data);
    }

    const editDispute = (sideVariable, id) => {
        console.log(sideVariable);
        console.log(id);
        let tempReqObj = {
            _id: id,
            side: sideVariable
        }
        socket.emit("getVotes", tempReqObj);
    }

    const getData = (message) => {
        console.log(message);
        setMsg(message);
    }

    return (
        <div>

            <Header
                setParentAddress={(address) => { setAddress(address) }}
                setEthersProvider={(provider) => { setProvider(provider) }}
            />

            {address
                ?
                <React.Fragment>
                    <main>
                        <div className={classes.vspace}>
                            <Container maxWidth="md">
                                <Grid container spacing={4}>
                                    {disputes.map((dispute, i) => (
                                        <Card className={classes.card}>
                                            <CardContent className={classes.cardContent}>
                                                <Typography>
                                                    Owner : {dispute.ownerSide}
                                                </Typography>
                                                <Typography>
                                                    Borrower : {dispute.borrowerSide}
                                                </Typography>
                                                <Typography>
                                                    Description : {dispute.description}
                                                </Typography>
                                            </CardContent>
                                            <Button variant="contained" color="primary" onClick={() => { editDispute("ownerSide", dispute._id) }}>
                                                Owner
                                            </Button>
                                            <Button variant="contained" color="primary" onClick={() => { editDispute("borrowerSide", dispute._id) }}>
                                                Borrower
                                            </Button>
                                        </Card>
                                    ))}
                                </Grid>
                            </Container>
                        </div>
                    </main>
                </React.Fragment>
                :
                <Fragment>
                    <div className={classes.loadingbar}></div>
                    <main className={classes.layout}>
                        <Paper className={classes.paper}>
                            <Typography component="h1" variant="h4" align="center">
                                Please connect with a wallet to continue
                            </Typography>
                        </Paper>
                    </main>
                </Fragment>
            }
        </div>
    );

}

export default Voting;