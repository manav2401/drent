import React, { useState, useEffect, Fragment } from 'react';
import { Link, Link as RouterLink, useHistory } from 'react-router-dom';
import {
    TextField,
    Grid,
    Button,
    IconButton,
    Paper,
    Typography,
    Snackbar,
    Card,
    CardContent,
    CardMedia,
    Container,
    Modal
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import MenuIcon from '@material-ui/icons/Menu';
import { ethers } from 'ethers';
import axios from 'axios';
import moment from 'moment';

import Header from '../common/header';
import { useStyles } from '../common/styles';
import { drentContractAddress, drentContractAbi } from '../../artifacts/DrentContract';
import { FETCH_DISPUTE } from '../../artifacts/uris'

const IpfsHttpClient = require('ipfs-http-client')

function getModalStyle() {
    return {
        top: '50%',
        left: '50%',
        transform: `translate(-50%, -50%)`,
    };
}

function ViewDisputes() {

    // use styles
    const classes = useStyles();
    const [modalStyle] = React.useState(getModalStyle);

    // use history
    const history = useHistory();

    // user wallet address
    const [address, setAddress] = useState(null);
    const [provider, setProvider] = useState(null);
    const [contract, setContract] = useState(null);

    // ipfs client
    const [ipfsClient, setIpfsClient] = useState(null);

    // disputes
    const [disputes, setDisputes] = useState([]);

    useEffect(async () => {

        if (provider && address) {

            const ipfsClient = new IpfsHttpClient({
                url: new URL('https://ipfs.infura.io:5001')
            });

            setIpfsClient(ipfsClient);

            // fetch disputes
            let options = {
                method: "GET",
                url: FETCH_DISPUTE
            }

            axios(options).then(response => {
                // console.log("disputes:", response.data.response);
                setDisputes(response.data.response)
            })

        }

    }, [provider, address]);

    return (
        <div>

            <Header
                setParentAddress={(address) => { setAddress(address) }}
                setEthersProvider={(provider) => { setProvider(provider) }}
            />

            {address
                ?
                (disputes.length != 0)
                    ?
                    <div className={classes.vspace}>
                        <Container maxWidth="md">
                            <Grid container spacing={4}>
                                {disputes.map((dispute, i) => (
                                    { dispute }
                                        ?
                                        <Grid item key={dispute._id} xs={12} sm={6} md={6}>
                                            <div>
                                                <Card className={classes.card}>
                                                    <CardContent className={classes.cardContent}>
                                                        <Typography gutterBottom variant="h5" component="h2">
                                                            {dispute.title}
                                                        </Typography>
                                                        <Typography>
                                                            Start Date: {moment(dispute.challengePeriodEndDate).subtract(7, 'days').format("DD/MM/YYYY")}
                                                        </Typography>
                                                        <Typography>
                                                            End Date: {moment(dispute.challengePeriodEndDate).format("DD/MM/YYYY")}
                                                        </Typography>
                                                    </CardContent>

                                                    <div className={classes.sidediv}>
                                                        <div className={classes.submit}>
                                                            <RouterLink to={`/app/orders/${dispute.orderId}/dispute`}>
                                                                <Button variant="contained" color="primary" className={classes.hspace}>
                                                                    Show More
                                                                </Button>
                                                            </RouterLink>
                                                        </div>
                                                    </div>

                                                </Card>
                                            </div>
                                        </Grid>
                                        :
                                        <div></div>
                                ))}

                            </Grid>
                        </Container>
                    </div>
                    :
                    <div>
                        <Typography component="h6" variant="h6" align="center">
                            <div>
                                No disputes to display
                        </div>
                        </Typography>
                        <div className={classes.sidediv}>
                            <div className={classes.submit}>
                                <RouterLink to='/app'>
                                    <Button
                                        type="submit"
                                        variant="outlined"
                                        color="primary"
                                        className={classes.hspace}
                                    >
                                        Go to Dashboard
                                    </Button>
                                </RouterLink>
                            </div>
                        </div>
                    </div>

                :
                <Fragment>
                    <div className={classes.loadingbar}></div>
                    <main className={classes.layout}>
                        <Paper className={classes.paper}>
                            <Typography component="h1" variant="h4" align="center">
                                Please connect with a wallet to continue
                            </Typography>
                        </Paper>
                    </main>
                </Fragment>
            }
        </div>
    );

}

export default ViewDisputes;