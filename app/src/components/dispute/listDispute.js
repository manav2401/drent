import React, { useState, useEffect, Fragment } from 'react';
import { Link as RouterLink, useParams } from 'react-router-dom';
import {
    TextField,
    Grid,
    AppBar,
    Button,
    Checkbox,
    IconButton,
    Paper,
    Toolbar,
    Typography,
    LinearProgress,
    Snackbar,
    Container,
    Card,
    CardContent
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import MenuIcon from '@material-ui/icons/Menu';
import LaunchIcon from '@material-ui/icons/Launch';
import { ethers } from 'ethers';
import axios from 'axios';
import moment from 'moment';
import socketIOClient from "socket.io-client";

import Header from '../common/header';
import { useStyles } from '../common/styles';
import { drentContractAddress, drentContractAbi } from '../../artifacts/DrentContract';
import { daiContractAddress, daiContractAbi } from '../../artifacts/DaiContract';
import { GET_DISPUTE_BY_ORDER_ID, GET_DISPUTE_CLAIM_STATUS, UPDATE_REPUTATION } from '../../artifacts/uris';


const IpfsHttpClient = require('ipfs-http-client')

function ListDispute() {

    // use styles
    const classes = useStyles();

    // use params
    const params = useParams();

    // socket
    const socket = socketIOClient("http://localhost:5000/");

    // user wal3let address
    const [address, setAddress] = useState(null);
    const [provider, setProvider] = useState(null);
    const [contract, setContract] = useState(null);

    // ipfs client
    const [ipfsClient, setIpfsClient] = useState(null);

    // params
    const [orderId, setOrderId] = useState(params.orderId);

    // dispute details
    const [description, setDescription] = useState(null);
    const [image, setImage] = useState(null);
    const [claim, setClaim] = useState(null);
    const [dispute, setDispute] = useState(null);
    const [isDisputeActive, setIsDisputeActive] = useState(null);
    const [hasVoted, setHasVoted] = useState(false);
    const [hasClaimed, setHasClaimed] = useState(false);
    const [hasVotedInitiator, setHasVotedInitiator] = useState(false);
    const [hasVotedOpposition, setHasVotedOpposition] = useState(false);
    const [claimAmount, setClaimAmount] = useState(null);
    const [reload, setReload] = useState(false);
    const [isWinner, setIsWinner] = useState(false);
    // other params
    const [loading, setLoading] = useState(false);
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState(null);

    const testObj = {
        'title': "title",
        'initiatorDescription': description,
        'initiatorImage': image,
        'initiatorClaim': claim,
        'challengeAmount': "challenge",
        'orderId': '',
        'initiatorAddress': '',
        'oppositionAddress': '',
        'registrationPeriodEndDate': moment().add(1, 'days').valueOf(),
        'oppositionDescription': '',
        'oppositionImage': '',
        'oppositionClaim': ''
    }

    useEffect(async () => {

        if (provider && address) {

            const ipfsClient = new IpfsHttpClient({
                url: new URL('https://ipfs.infura.io:5001')
            });

            setIpfsClient(ipfsClient);

            // get signer
            const signer = provider.getSigner();

            // create contract instance
            const drentContract = new ethers.Contract(drentContractAddress, drentContractAbi, signer);
            setContract(drentContract);

            // check if order & dispute exists or not
            const options = {
                method: "GET",
                url: GET_DISPUTE_BY_ORDER_ID + "/" + orderId,
            };

            console.log(options);

            axios(options).then(response => {
                if (response.status == 200) {

                    let dispute = response.data.data;
                    // console.log("Dispute for order:", response.data.data);
                    setDispute(response.data.data);

                    // call status api
                    const options = {
                        method: "GET",
                        // url: GET_DISPUTE_CLAIM_STATUS + "?orderId=" + String(orderId) + "&address=" + String(address),
                        url: GET_DISPUTE_CLAIM_STATUS,
                        params: {
                            orderId: String(orderId),
                            address: String(address)
                        }
                    }

                    axios(options).then(response => {

                        // console.log("Dispute Claim Status:", response.data);

                        // check dispute period
                        const currentDate = new moment();
                        const registrationPeriodEndDate = moment.unix(dispute.registrationPeriodEndDate / 1000);

                        if (currentDate.isBefore(registrationPeriodEndDate)) {

                            console.log("dispute active...")

                            // dispute active
                            setIsDisputeActive(true);

                            // check if it's initiator/challenger or other user
                            if (dispute.initiatorAddress == address || dispute.oppositionAddress == address) {
                                setHasVoted(true);
                            } else {
                                setHasVoted(response.data.hasVoted);
                            }


                        } else {

                            console.log("dispute inactive...")

                            // dispute inactive
                            setIsDisputeActive(false);

                            // check if it's initiator/challenger or other user
                            if (dispute.initiatorAddress == address || dispute.oppositionAddress == address) {

                                setHasVoted(true);
                                setHasClaimed(false);

                                if (dispute.initiatorVotes >= dispute.oppositionVotes) {

                                    setHasVotedInitiator(dispute.initiatorAddress == address ? true : false);
                                    setHasVotedOpposition(dispute.initiatorAddress == address ? false : true);

                                    let amount = 0;
                                    amount = Number(dispute.challengeAmount) + Number(dispute.initiatorAddress == address ? dispute.initiatorClaim : 0);
                                    setClaimAmount(Number(amount).toFixed(4));

                                    // set winner 
                                    setIsWinner(dispute.initiatorAddress == address ? true : false);

                                } else {

                                    setHasVotedInitiator(dispute.initiatorAddress == address ? true : false);
                                    setHasVotedOpposition(dispute.initiatorAddress == address ? false : true);

                                    let amount = 0;
                                    amount = Number(dispute.challengeAmount) + Number(dispute.initiatorAddress == address ? dispute.oppositionClaim : dispute.initiatorClaim - dispute.oppositionClaim);
                                    setIsWinner(true);
                                    setClaimAmount(Number(amount).toFixed(4));

                                    // set winner 
                                    setIsWinner(dispute.initiatorAddress == address ? false : true);

                                }

                            } else {
                                // set voted variable
                                setHasVoted(response.data.hasVoted);

                                // set claimed variable
                                setHasClaimed(response.data.claimed);
                            }

                            if (response.data.vote == dispute.initiatorAddress) {

                                // voted initiator
                                setHasVotedInitiator(true);
                                setHasVotedOpposition(false);

                                let amount = 0;

                                // calculate claim amount
                                if (response.data.winner) {
                                    setIsWinner(true);
                                    amount = 5 + ((dispute.oppositionVotes) / (dispute.initiatorVotes));

                                } else {
                                    setIsWinner(false);
                                    amount = 4;

                                }

                                setClaimAmount(Number(amount).toFixed(4));
                                console.log(amount);


                            } else if (response.data.vote == dispute.oppositionAddress) {

                                // voted opposition 
                                setHasVotedOpposition(true);
                                setHasVotedInitiator(false);

                                let amount = 0;

                                // calculate claim amount
                                if (response.data.winner) {
                                    setIsWinner(true);
                                    amount = 5 + ((dispute.initiatorVotes) / (dispute.oppositionVotes));

                                } else {
                                    setIsWinner(false);
                                    amount = 4;

                                }

                                setClaimAmount(Number(amount).toFixed(4));
                                console.log(amount);


                            } else {

                                // not voted (hasVoted false in case of not voted)

                            }

                        }

                    }).catch(error => {
                        console.log("Error in fetching claim status for dispute:", error);
                    })

                }
            }).catch(error => {
                console.log("Error in fetching dispute for order:", error);
            });

            // socket.emit("allVotes");
            socket.on("get_data", setData);
            socket.on("change_vote", setNewData);
            socket.on("claim_change", printMessage);
        }

    }, [provider, address, reload]);

    const printMessage = (data) => {
        console.log("Status of claim : ", data["messaged"]);
    }

    const setData = (data) => {
        console.log("Heyy");
        console.log(data);
        setDispute(data);
    }

    const voteUser = async (e, sideVariable, id, _address) => {

        e.preventDefault();

        if (isDisputeActive && !hasVoted) {

            // make payment (5 DAI)
            setLoading(true);

            // get signer
            const signer = provider.getSigner();

            // create dai contract instance
            const daiContract = new ethers.Contract(daiContractAddress, daiContractAbi, signer);

            let approveResponse = null;
            let approveReceipt = null;
            let acceptFundsResponse = null;
            let acceptFundsReceipt = null;

            try {

                // call the drent contract to transfer funds
                approveResponse = await daiContract.approve(drentContractAddress, 50000);

                // wait for confirmation
                approveReceipt = await approveResponse.wait();

                // value to pay: challenge amount for creating dispute
                let overrides = {
                    value: String(50000)
                };
                acceptFundsResponse = await contract.acceptFunds(overrides);

                // wait for confirmation
                acceptFundsReceipt = await acceptFundsResponse.wait();
            } catch (error) {
                console.log("Error in payment:", error);
            }

            if (acceptFundsResponse) {

                // user is yet to vote
                // console.log(sideVariable);
                // console.log(id);
                let tempReqObj = {
                    _id: id,
                    side: sideVariable,
                    address: _address
                }
                socket.emit("getVotes", tempReqObj);

                let event = null;
                if (isWinner) {
                    event = "winner";
                }
                else {
                    event = "looser";
                }
                let body = {
                    "address": address,
                    "event": event
                }

                const opt = {
                    method: "POST",
                    url: UPDATE_REPUTATION,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json;charset=UTF-8',
                    },
                    data: body
                };

                axios(opt).then(res => {
                    console.log("Reputation updated");
                }).catch(error => {
                    console.log("Error in updating reputation", error);
                })

                setLoading(false);
                setSnackbarOpen(true);
                setSnackbarMessage("Successfully Voted");

                setReload(reload ? false : true);

            } else {

                setLoading(false);
                setSnackbarOpen(true);
                setSnackbarMessage("Unable to vote. Try again later.");

            }

        } else if (!isDisputeActive && !hasClaimed) {

            // claim funds back
            setLoading(true);

            // get signer
            const signer = provider.getSigner();

            // create dai contract instance
            const daiContract = new ethers.Contract(daiContractAddress, daiContractAbi, signer);

            let claimFundsResponse = null;
            let claimFundsReceipt = null;

            try {

                claimFundsResponse = await contract.sendFunds(address, claimAmount * 10000);

                // wait for confirmation
                claimFundsReceipt = await claimFundsResponse.wait();
            } catch (error) {
                console.log("Error in claim funds:", error);
            }

            if (claimFundsReceipt) {

                // user is yet to claim
                let tempReqObj = {
                    _id: id,
                    address: _address
                }
                socket.emit("claim", tempReqObj);

                let body = {
                    "address": address,
                    "result": ""
                }

                const options = {
                    method: "POST",
                    url: UPDATE_REPUTATION,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json;charset=UTF-8',
                    },
                    data: body
                };

                axios(options).then(response => {
                    console.log("Reputation updated");
                }).catch(error => {
                    console.log("Error in updating reputation", error);
                })


                setLoading(false);
                setSnackbarOpen(true);
                setSnackbarMessage("Claim Successfull");

                setReload(reload ? false : true);

            } else {

                setLoading(false);
                setSnackbarOpen(true);
                setSnackbarMessage("Unable to claim funds. Try again later.");

            }

        }

    }


    const setNewData = (updatedDispute) => {
        console.log(updatedDispute);
        setDispute(updatedDispute);
    }

    const handleDescriptionChange = (e) => {
        setDescription(e.target.value);
    }

    const handleImageChange = (e) => {
        setImage(e.target.value);
    }

    const handleClaimChange = (e) => {
        setClaim(e.target.value);
    }

    const onDisputeSave = async (e) => {

        e.preventDefault();

        // create json data
        let disputeJson = {
            'title': "title",
            'initiatorDescription': description,
            'initiatorImage': image,
            'initiatorClaim': claim,
            'challengeAmount': "challenge",
            'orderId': '',
            'initiatorAddress': '',
            'oppositionAddress': '',
            'registrationPeriodEndDate': moment().add(1, 'days').valueOf(),
            'oppositionDescription': '',
            'oppositionImage': '',
            'oppositionClaim': '',
            'ownerSide': 0,
            'borrowerSide': 0
        };
        setDispute(disputeJson);
        setLoading(true);

    }

    // handle snackbar close
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnackbarOpen(false);
    };

    return (
        <div>
            <Header
                setParentAddress={(address) => { setAddress(address) }}
                setEthersProvider={(provider) => { setProvider(provider) }}
            />

            {address
                ?
                dispute
                    ?
                    <React.Fragment>
                        <form noValidate autoComplete="off">
                            <main className={classes.layout}>
                                <div className={classes.loadingbar}>
                                    {loading ? <LinearProgress /> : null}
                                </div>

                                <Paper className={classes.paper}>
                                    <Typography component="h1" variant="h4" align="center">
                                        <div>
                                            {dispute.title}
                                        </div>
                                    </Typography>

                                    <Typography component="h6" variant="h6" align="center" style={{ paddingTop: '10px' }} >
                                        <div>
                                            Start Date: {moment(dispute.challengePeriodEndDate).subtract(7, 'days').format("DD/MM/YYYY")}, End Date: {moment(dispute.challengePeriodEndDate).format("DD/MM/YYYY")}
                                        </div>
                                    </Typography>

                                    <Typography component="h6" variant="h6" align="center" style={{ paddingTop: '10px' }} >
                                        <div>
                                            Voting Fee: 5 DAI
                                        </div>
                                    </Typography>

                                    <Typography component="h6" variant="h6" align="center" style={{ paddingTop: '10px' }} >
                                        <div>
                                            View order here
                                            <RouterLink to={`/app/orders/${dispute.orderId}`}>
                                                <LaunchIcon fontSize="small" />
                                            </RouterLink>
                                        </div>
                                    </Typography>

                                    <Typography component="h6" variant="h6" align="center" style={{ paddingTop: '10px' }} >
                                        {isDisputeActive
                                            ?
                                            <div></div>
                                            :
                                            <Typography>
                                                Dispute has ended
                                            </Typography>
                                        }
                                    </Typography>
                                </Paper>

                                <Paper className={classes.paper}>
                                    <Typography component="h1" variant="h4" align="center">
                                        <div>
                                            Dispute Initiator
                                            </div>
                                    </Typography>

                                    <div style={{ justifyContent: 'space-evenly' }} className={classes.sidediv}>

                                        {/* Image */}
                                        <div className={classes.updiv}>
                                            <img src={dispute.initiatorImage}></img>
                                        </div>

                                        {/* Dispute Details */}
                                        <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                                            <div className={classes.sidediv} style={{ justifyContent: 'space-between' }}>
                                                <div className={classes.updiv}>
                                                    <div>Created By: </div>
                                                    <div>Description: </div>
                                                    <div>Claim Amount: </div>
                                                    <div>Votes: </div>
                                                </div>
                                                <div className={classes.updiv}>
                                                    <div>{dispute.initiatorAddress}</div>
                                                    <div>{dispute.initiatorDescription}</div>
                                                    <div>{dispute.initiatorClaim} DAI</div>
                                                    <div>{dispute.initiatorVotes}</div>
                                                </div>
                                            </div>
                                        </Typography>
                                    </div>

                                    {
                                        isDisputeActive
                                            ?
                                            <Button
                                                type="submit"
                                                variant="contained"
                                                color="primary"
                                                className={classes.button}
                                                disabled={hasVoted}
                                                onClick={(e) => { voteUser(e, "initiatorVotes", dispute._id, address) }}
                                            >
                                                Vote (5 DAI)
                                                                        </Button>
                                            :
                                            hasVoted
                                                ?
                                                <Button
                                                    type="submit"
                                                    variant="contained"
                                                    color="primary"
                                                    className={classes.button}
                                                    disabled={!(hasVotedInitiator && !hasClaimed)}
                                                    onClick={(e) => { voteUser(e, "initiatorVotes", dispute._id, address) }}
                                                >
                                                    {(hasVotedInitiator) ? "Claim Funds (" + claimAmount + " DAI)" : "Vote (5 DAI)"}
                                                </Button>
                                                :
                                                <Button
                                                    type="submit"
                                                    variant="contained"
                                                    color="primary"
                                                    className={classes.button}
                                                    disabled={true}
                                                    onClick={(e) => { voteUser(e, "initiatorVotes", dispute._id, address) }}
                                                >
                                                    Vote (5 DAI)
                                                    </Button>

                                    }

                                </Paper>

                                <Paper className={classes.paper}>
                                    <Typography component="h1" variant="h4" align="center">
                                        <div>
                                            Dispute Challenger
                                            </div>
                                    </Typography>

                                    <div style={{ justifyContent: 'space-evenly' }} className={classes.sidediv}>

                                        {/* Image */}
                                        <div className={classes.updiv}>
                                            <img src={dispute.oppositionImage}></img>
                                        </div>

                                        {/* Dispute Details */}
                                        <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                                            <div className={classes.sidediv} style={{ justifyContent: 'space-between' }}>
                                                <div className={classes.updiv}>
                                                    <div>Challenged By: </div>
                                                    <div>Description: </div>
                                                    <div>Claim Amount: </div>
                                                    <div>Votes: </div>
                                                </div>
                                                <div className={classes.updiv}>
                                                    <div>{dispute.oppositionAddress}</div>
                                                    <div>{dispute.oppositionDescription}</div>
                                                    <div>{dispute.oppositionClaim} DAI</div>
                                                    <div>{dispute.oppositionVotes}</div>
                                                </div>
                                            </div>
                                        </Typography>

                                    </div>

                                    {
                                        isDisputeActive
                                            ?
                                            <Button
                                                type="submit"
                                                variant="contained"
                                                color="primary"
                                                className={classes.button}
                                                disabled={hasVoted}
                                                onClick={(e) => { voteUser(e, "oppositionVotes", dispute._id, address) }}
                                            >
                                                Vote (5 DAI)
                                                                        </Button>
                                            :
                                            hasVoted
                                                ?
                                                <Button
                                                    type="submit"
                                                    variant="contained"
                                                    color="primary"
                                                    className={classes.button}
                                                    disabled={!(hasVotedOpposition && !hasClaimed)}
                                                    onClick={(e) => { voteUser(e, "oppositionVotes", dispute._id, address) }}
                                                >
                                                    {(hasVotedOpposition) ? "Claim Funds (" + claimAmount + " DAI)" : "Vote (5 DAI)"}
                                                </Button>
                                                :
                                                <Button
                                                    type="submit"
                                                    variant="contained"
                                                    color="primary"
                                                    className={classes.button}
                                                    disabled={true}
                                                    onClick={(e) => { voteUser(e, "oppositionVotes", dispute._id, address) }}
                                                >
                                                    Vote (5 DAI)
                                                    </Button>

                                    }

                                </Paper>
                                <div className={classes.sidediv} style={{ marginBottom: "20px" }}>
                                    <div className={classes.hspace}>
                                        <RouterLink to='/app'>
                                            <Button
                                                type="submit"
                                                variant="outlined"
                                                color="primary"
                                                className={classes.button}
                                            // to={'/app'}
                                            // onClick={(e) => { onProfileSave(e) }}
                                            >
                                                Go to Dashboard
                                        </Button>
                                        </RouterLink>
                                    </div>
                                    <div className={classes.hspace}>
                                        <RouterLink to='/app/products/add'>
                                            <Button
                                                type="submit"
                                                variant="outlined"
                                                color="primary"
                                                className={classes.button}
                                            // onClick={(e) => { onProfileSave(e) }}
                                            >
                                                Add a product on rent
                                        </Button>
                                        </RouterLink>
                                    </div>
                                </div>
                            </main>
                        </form>
                    </React.Fragment>
                    :
                    <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                        <div>
                            Unable to fetch dispute for Order: {orderId}. Try again later.
                            </div>
                        <div className={classes.sidediv}>
                            <RouterLink to='/app'>
                                <Button
                                    type="submit"
                                    variant="outlined"
                                    color="primary"
                                    className={classes.hspace}
                                // onClick={(e) => { onProfileSave(e) }}
                                >
                                    Back to Dashboard
                            </Button>
                            </RouterLink>
                        </div>
                    </Typography>
                :
                <Fragment>
                    <div className={classes.loadingbar}></div>
                    <main className={classes.layout}>
                        <Paper className={classes.paper}>
                            <Typography component="h1" variant="h4" align="center">
                                Please connect with a wallet to continue
                            </Typography>
                        </Paper>
                    </main>
                </Fragment>
            }

            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                open={snackbarOpen}
                autoHideDuration={4000}
                onClose={handleClose}
                message={snackbarMessage}
                action={
                    <React.Fragment>
                        <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                            <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment>
                }
            />
        </div>
    );
}

export default ListDispute;