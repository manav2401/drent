import React, { useState, useEffect, Fragment } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import {
    TextField,
    Grid,
    AppBar,
    Button,
    Checkbox,
    IconButton,
    Paper,
    Toolbar,
    Typography,
    LinearProgress,
    Snackbar
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import MenuIcon from '@material-ui/icons/Menu';
import { ethers } from 'ethers';
import axios from 'axios';
import moment from 'moment';

import Header from '../common/header';
import { useStyles } from '../common/styles';
import { drentContractAddress, drentContractAbi } from '../../artifacts/DrentContract';
import { daiContractAddress, daiContractAbi } from '../../artifacts/DaiContract';
import { CREATE_DISPUTE, UPDATE_REPUTATION } from '../../artifacts/uris';

const IpfsHttpClient = require('ipfs-http-client')

function CreateDispute(props) {

    // use styles
    const classes = useStyles();

    // user wallet address
    const [address, setAddress] = useState(null);
    const [provider, setProvider] = useState(null);
    const [contract, setContract] = useState(null);

    // ipfs client
    const [ipfsClient, setIpfsClient] = useState(null);

    // dispute details
    const [title, setTitle] = useState(null);
    const [description, setDescription] = useState(null);
    const [image, setImage] = useState(null);
    const [claim, setClaim] = useState(null);
    const [challenge, setChallenge] = useState(null);
    const [dispute, setDispute] = useState(null);
    const [userHash, setUserHash] = useState(null);

    // props
    const [orderId, setOrderId] = useState(null);
    const [initiatorAddress, setInitiatorAddress] = useState(null);
    const [oppositionAddress, setOppositionAddress] = useState(null);

    // other params
    const [loading, setLoading] = useState(false);
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState(null);

    useEffect(async () => {

        if (provider && props.location.state) {

            const ipfsClient = new IpfsHttpClient({
                url: new URL('https://ipfs.infura.io:5001')
            });

            setIpfsClient(ipfsClient);

            // fetch props
            setOrderId(props.location.state.orderId);
            setInitiatorAddress(props.location.state.initiatorAddress);
            setOppositionAddress(props.location.state.oppositionAddress);

            // console.log(props.location.state.orderId);
            // console.log(props.location.state.initiatorAddress);
            // console.log(props.location.state.oppositionAddress);

            // get signer
            const signer = provider.getSigner();

            // create contract instance
            const drentContract = new ethers.Contract(drentContractAddress, drentContractAbi, signer);
            setContract(drentContract);

            // check if the profile exists or not
            let userProfile = null;
            try {
                userProfile = await drentContract.getUser();
            } catch (error) {
                // no profile exists
                console.log("Error in fetching profile:", error.message);
            }

            // if (userProfile) {

            //     // fetch the user object
            //     const url = "https://ipfs.io/ipfs/" + String(userProfile);
            //     axios.get(url).then(response => {

            //         // set the user details from object
            //         response = response.data;

            //     }).catch(error => {
            //         console.log("Error in fetching user from hash:", error);
            //     })

            // }

        }

    }, [provider]);

    const handleTitleChange = (e) => {
        setTitle(e.target.value);
    }

    const handleDescriptionChange = (e) => {
        setDescription(e.target.value);
    }

    const handleImageChange = (e) => {
        setImage(e.target.value);
    }

    const handleClaimChange = (e) => {
        setClaim(e.target.value);
    }

    const handleChallengeChange = (e) => {
        setChallenge(e.target.value);
    }

    const onDisputeSave = async (e) => {

        e.preventDefault();

        setLoading(true);

        // get signer
        const signer = provider.getSigner();

        // create dai contract instance
        const daiContract = new ethers.Contract(daiContractAddress, daiContractAbi, signer);

        let approveResponse = null;
        let approveReceipt = null;
        let acceptFundsResponse = null;
        let acceptFundsReceipt = null;

        try {

            // call the drent contract to transfer funds
            approveResponse = await daiContract.approve(drentContractAddress, challenge * 10000);

            // wait for confirmation
            approveReceipt = await approveResponse.wait();

            // value to pay: challenge amount for creating dispute
            let overrides = {
                value: String(challenge * 10000)
            };
            acceptFundsResponse = await contract.acceptFunds(overrides);

            // wait for confirmation
            acceptFundsReceipt = await acceptFundsResponse.wait();
        } catch (error) {
            console.log("Error in payment:", error);
        }

        if (acceptFundsResponse) {

            // create json data
            let dispute = {
                'title': title,
                'initiatorDescription': description,
                'initiatorImage': image,
                'initiatorClaim': claim,
                'challengeAmount': challenge,
                'orderId': orderId,
                'initiatorAddress': initiatorAddress,
                'oppositionAddress': oppositionAddress,
                'challengePeriodEndDate': moment().add(1, 'days').valueOf(),
                'challenged': false,
                'initiatorVotes': 0,
                'oppositionVotes': 0,
                'initiatorSideAddress': [],
                'oppositionSideAddress': [],
                'claimAddress': []
            };
            setDispute(dispute);

            const options = {
                method: "POST",
                url: CREATE_DISPUTE,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json;charset=UTF-8',
                },
                data: dispute
            };

            axios(options).then(response => {

                let body = {
                    "address": address,
                    "event": "dispute"
                }

                const opt = {
                    method: "POST",
                    url: UPDATE_REPUTATION,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json;charset=UTF-8',
                    },
                    data: body
                };

                axios(opt).then(res => {
                    console.log("Reputation updated");
                }).catch(error => {
                    console.log("Error in updating reputation", error);
                })

                // console.log("Dispute Created...", response);
                setSnackbarMessage("Dispute Created");
            }).catch(error => {
                // console.log("Error in creating dispute", error);
                setSnackbarMessage("Unable to create dispute. Try again later.");
            })

        } else {
            // console.log("Error in adding dispute...");
            setSnackbarMessage("Unable to create dispute. Try again later.");
        }

        setLoading(false);
        setSnackbarOpen(true);

    }

    const uploadImage = async (e) => {
        const file = e.target.files[0];
        try {
            const base64 = await convertBase64(file);
            // console.log('image uploaded: ' + base64);
            setImage(null);
            setImage(base64);
        } catch (error) {
            console.log("Error in image upload");
            alert("Error in uploading image. Please try again.");
        }
    };

    const convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);

            fileReader.onload = () => {
                resolve(fileReader.result);
            };

            fileReader.onerror = (error) => {
                reject(error);
            };
        });
    };

    // handle snackbar close
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnackbarOpen(false);
    };

    const DisputeForm = (
        <React.Fragment>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <TextField
                        required
                        id="title"
                        name="title"
                        label="Title"
                        fullWidth
                        value={title || ''}
                        onChange={handleTitleChange}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        required
                        id="initiatorDescription"
                        name="initiatorDescription"
                        label="Description"
                        fullWidth
                        value={description || ''}
                        onChange={handleDescriptionChange}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        required
                        id="initiatorClaim"
                        name="initiatorClaim"
                        label="Claim amount (valid upto 4 decimal places)"
                        fullWidth
                        value={claim || ''}
                        onChange={handleClaimChange}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        required
                        id="challengeAmount"
                        name="challengeAmount"
                        label="Challenge Amount (valid upto 4 decimal places)"
                        fullWidth
                        value={challenge || ''}
                        onChange={handleChallengeChange}
                    />
                </Grid>
                <Grid item xs={12}>
                    <div className={classes.sidediv}>
                        <div className={classes.hspace}>
                            Upload an image as a proof
                        </div>
                        <input className={classes.hspace}
                            type="file"
                            onChange={(e) => { uploadImage(e); }}
                        />
                    </div>
                </Grid>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    // className={classes.submit}
                    className={classes.button}
                    onClick={(e) => { onDisputeSave(e) }}
                >
                    Create Dispute {challenge ? '(' + challenge + ' DAI)' : ''}
                </Button>
            </Grid>
        </React.Fragment>
    )

    return (
        <div>
            <Header
                setParentAddress={(address) => { setAddress(address) }}
                setEthersProvider={(provider) => { setProvider(provider) }}
            />

            {address
                ?
                (orderId)
                    ?
                    <React.Fragment>
                        <form noValidate autoComplete="off">
                            <main className={classes.layout}>
                                <div className={classes.loadingbar}>
                                    {loading ? <LinearProgress /> : null}
                                </div>
                                <Paper className={classes.paper}>
                                    <Typography component="h1" variant="h4" align="center">
                                        <div>
                                            {/* {address ? ('Create a new dispute for ' + address) : ''} */}
                                            Create a new dispute
                                        </div>
                                    </Typography>

                                    <Typography component="h6" variant="h6" align="center">
                                        <div>
                                            Order ID: {orderId}
                                        </div>
                                    </Typography>

                                    <React.Fragment>
                                        {DisputeForm}
                                    </React.Fragment>
                                </Paper>
                                <div className={classes.sidediv}>
                                    <div className={classes.submit}>
                                        <RouterLink to='/app/orders'>
                                            <Button
                                                type="submit"
                                                variant="outlined"
                                                color="primary"
                                                className={classes.hspace}
                                            // onClick={(e) => { onProfileSave(e) }}
                                            >
                                                View Orders
                                        </Button>
                                        </RouterLink>
                                    </div>
                                    <div className={classes.submit}>
                                        <RouterLink to='/app'>
                                            <Button
                                                type="submit"
                                                variant="outlined"
                                                color="primary"
                                                className={classes.hspace}
                                            // onClick={(e) => { onProfileSave(e) }}
                                            >
                                                Go to Dashboard
                                        </Button>
                                        </RouterLink>
                                    </div>
                                    <div className={classes.submit}>
                                        <RouterLink to='/app/products/add'>
                                            <Button
                                                type="submit"
                                                variant="outlined"
                                                color="primary"
                                                className={classes.hspace}
                                            // onClick={(e) => { onProfileSave(e) }}
                                            >
                                                Add a product on rent
                                        </Button>
                                        </RouterLink>
                                    </div>
                                </div>
                            </main>
                        </form>
                    </React.Fragment>
                    :
                    <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                        <div>
                            Oops...Looks like you're lost.
                        </div>
                        <div className={classes.sidediv}>
                            <RouterLink to='/app'>
                                <Button
                                    type="submit"
                                    variant="outlined"
                                    color="primary"
                                    className={classes.hspace}
                                    to={'/app'}
                                // onClick={(e) => { onProfileSave(e) }}
                                >
                                    Back to Dashboard
                                </Button>
                            </RouterLink>
                        </div>
                    </Typography>
                :
                <Fragment>
                    <div className={classes.loadingbar}></div>
                    <main className={classes.layout}>
                        <Paper className={classes.paper}>
                            <Typography component="h1" variant="h4" align="center">
                                Please connect with a wallet to continue
                            </Typography>
                        </Paper>
                    </main>
                </Fragment>
            }

            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                open={snackbarOpen}
                autoHideDuration={4000}
                onClose={handleClose}
                message={snackbarMessage}
                action={
                    <React.Fragment>
                        <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                            <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment>
                }
            />


        </div>

    );

}

export default CreateDispute;