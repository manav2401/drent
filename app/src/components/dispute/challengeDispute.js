import React, { useState, useEffect, Fragment } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import {
    TextField,
    Grid,
    AppBar,
    Button,
    Checkbox,
    IconButton,
    Paper,
    Toolbar,
    Typography,
    LinearProgress,
    Snackbar,
    Container,
    Card,
    CardContent
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import MenuIcon from '@material-ui/icons/Menu';
import { ethers } from 'ethers';
import axios from 'axios';
import moment from 'moment';

import Header from '../common/header';
import { useStyles } from '../common/styles';
import { drentContractAddress, drentContractAbi } from '../../artifacts/DrentContract';
import { daiContractAddress, daiContractAbi } from '../../artifacts/DaiContract';
import { GET_DISPUTE_BY_ORDER_ID, CHALLENGE_DISPUTE, UPDATE_REPUTATION } from '../../artifacts/uris';

const IpfsHttpClient = require('ipfs-http-client')

function ChallengeDispute(props) {

    // use styles
    const classes = useStyles();

    // user wal3let address
    const [address, setAddress] = useState(null);
    const [provider, setProvider] = useState(null);
    const [contract, setContract] = useState(null);

    // ipfs client
    const [ipfsClient, setIpfsClient] = useState(null);

    // dispute details
    const [description, setDescription] = useState(null);
    const [image, setImage] = useState(null);
    const [claim, setClaim] = useState(null);
    const [dispute, setDispute] = useState(null);

    // props
    const [orderId, setOrderId] = useState(null);

    // other params
    const [loading, setLoading] = useState(false);
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState(null);

    const testObj = {
        'title': "title",
        'initiatorDescription': "description",
        'initiatorImage': "image",
        'initiatorClaim': 100,
        'challengeAmount': 50,
        'orderId': '',
        'initiatorAddress': '',
        'oppositionAddress': '',
        'registrationPeriodEndDate': moment().add(1, 'days').valueOf()
    }

    useEffect(async () => {

        if (provider && props.location.state) {

            const ipfsClient = new IpfsHttpClient({
                url: new URL('https://ipfs.infura.io:5001')
            });

            setIpfsClient(ipfsClient);

            // fetch props
            setOrderId(props.location.state.orderId);
            // console.log(props.location.state.orderId)

            // get signer
            const signer = provider.getSigner();

            // create contract instance
            const drentContract = new ethers.Contract(drentContractAddress, drentContractAbi, signer);
            setContract(drentContract);

            // check if the profile exists or not
            // let userProfile = null;
            // try {
            //     userProfile = await drentContract.getUser();
            // } catch (error) {
            //     // no profile exists
            //     console.log("Error in fetching profile:", error.message);
            // }

            // fetch dispute by order id
            const options = {
                method: "GET",
                url: GET_DISPUTE_BY_ORDER_ID + "/" + props.location.state.orderId,
            };

            // console.log(options);

            axios(options).then(response => {
                // console.log("Dispute for order:", response.data.data);
                setDispute(response.data.data);
            }).catch(error => {
                console.log("Error in fetching dispute for order:", error);
            });

            // if (userProfile) {

            //     // fetch the user object
            //     const url = "https://ipfs.io/ipfs/" + String(userProfile);
            //     axios.get(url).then(response => {

            //         // set the user details from object
            //         response = response.data;

            //     }).catch(error => {
            //         console.log("Error in fetching user from hash:", error);
            //     })

            // }

        }

    }, [provider]);

    const handleDescriptionChange = (e) => {
        setDescription(e.target.value);
    }

    const handleImageChange = (e) => {
        setImage(e.target.value);
    }

    const handleClaimChange = (e) => {
        setClaim(e.target.value);
    }

    const onDisputeSave = async (e) => {

        e.preventDefault();

        setLoading(true);

        // get signer
        const signer = provider.getSigner();

        // create dai contract instance
        const daiContract = new ethers.Contract(daiContractAddress, daiContractAbi, signer);

        let approveResponse = null;
        let approveReceipt = null;
        let acceptFundsResponse = null;
        let acceptFundsReceipt = null;

        try {

            // call the drent contract to transfer funds
            approveResponse = await daiContract.approve(drentContractAddress, dispute.challengeAmount * 10000);

            // wait for confirmation
            approveReceipt = await approveResponse.wait();

            // value to pay: challenge amount for challenging dispute
            let overrides = {
                value: String(dispute.challengeAmount * 10000)
            };
            acceptFundsResponse = await contract.acceptFunds(overrides);

            // wait for confirmation
            acceptFundsReceipt = await acceptFundsResponse.wait();
        } catch (error) {
            console.log("Error in payment:", error);
        }

        // console.log("Existing dispute:", dispute);

        if (acceptFundsResponse) {

            // create json data
            let updatedDispute = {
                'title': dispute.title,
                'initiatorDescription': dispute.initiatorDescription,
                'initiatorImage': dispute.initiatorImage,
                'initiatorClaim': dispute.initiatorClaim,
                'challengeAmount': dispute.challengeAmount,
                'orderId': orderId,
                'initiatorAddress': dispute.initiatorAddress,
                'oppositionAddress': dispute.oppositionAddress,
                'registrationPeriodEndDate': moment().add(7, 'days').valueOf(),
                'challengePeriodEndDate': moment().valueOf(),
                'oppositionDescription': description,
                'oppositionImage': image,
                'oppositionClaim': claim,
                'challenged': true,
                'initiatorVotes': dispute.initiatorVotes,
                'oppositionVotes': dispute.oppositionVotes,
                'initiatorSideAddress': dispute.initiatorSideAddress,
                'oppositionSideAddress': dispute.oppositionSideAddress,
                'claimAddress': dispute.claimAddress
            };

            // console.log("Updated Dispute:", updatedDispute);
            setDispute(updatedDispute);

            const options = {
                method: "PUT",
                url: CHALLENGE_DISPUTE,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json;charset=UTF-8',
                },
                data: updatedDispute
            };

            axios(options).then(response => {

                let body = {
                    "address": address,
                    "event": "challenge"
                }

                const opt = {
                    method: "POST",
                    url: UPDATE_REPUTATION,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json;charset=UTF-8',
                    },
                    data: body
                };

                axios(opt).then(res => {
                    console.log("Reputation updated");
                }).catch(error => {
                    console.log("Error in updating reputation", error);
                })

                // console.log("Challenge created...", response);
                setSnackbarMessage("Challenge Created");
            }).catch(error => {
                // console.log("Error in creating challenge", error);
                setSnackbarMessage("Unable to create challenge. Try again later.");
            })

        } else {
            // console.log("Error in adding dispute...");
            setSnackbarMessage("Unable to create challenge. Try again later.");
        }

        setLoading(false);
        setSnackbarOpen(true);

    }

    const uploadImage = async (e) => {
        const file = e.target.files[0];
        try {
            const base64 = await convertBase64(file);
            // console.log('image uploaded: ' + base64);
            setImage(null);
            setImage(base64);
        } catch (error) {
            console.log("Error in image upload");
            alert("Error in uploading image. Please try again.");
        }
    };

    const convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);

            fileReader.onload = () => {
                resolve(fileReader.result);
            };

            fileReader.onerror = (error) => {
                reject(error);
            };
        });
    };

    // handle snackbar close
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnackbarOpen(false);
    };

    const DisputeForm = (
        <React.Fragment>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <TextField
                        required
                        id="oppositionDescription"
                        name="oppositionDescription"
                        label="Description"
                        fullWidth
                        value={description || ''}
                        onChange={handleDescriptionChange}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        required
                        id="oppositionClaim"
                        name="oppositionClaim"
                        label="Claim amount (valid upto 4 decimal places)"
                        fullWidth
                        value={claim || ''}
                        onChange={handleClaimChange}
                    />
                </Grid>
                <Grid item xs={12}>
                    <div className={classes.sidediv}>
                        <div className={classes.hspace}>
                            Upload an image as a proof
                        </div>
                        <input className={classes.hspace}
                            type="file"
                            onChange={(e) => { uploadImage(e); }}
                        />
                    </div>
                </Grid>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    // className={classes.submit}
                    className={classes.button}
                    onClick={(e) => { onDisputeSave(e) }}
                >
                    Create Challenge {dispute ? '(' + dispute.challengeAmount + ' DAI)' : ''}
                </Button>
            </Grid>
        </React.Fragment>
    )

    return (
        <div>
            <Header
                setParentAddress={(address) => { setAddress(address) }}
                setEthersProvider={(provider) => { setProvider(provider) }}
            />

            {address
                ?
                orderId
                    ?
                    dispute
                        ?
                        <React.Fragment>
                            <form noValidate autoComplete="off">
                                <main className={classes.layout}>
                                    <div className={classes.loadingbar}>
                                        {loading ? <LinearProgress /> : null}
                                    </div>
                                    <Paper className={classes.paper}>
                                        <Typography component="h5" variant="h5" align="center">
                                            <div>
                                                Create a challenge
                                                {/* {orderId ? ('Dispute for Order:' + orderId) : ''} */}
                                            </div>
                                        </Typography>

                                        <Typography component="h6" variant="h6" align="center">
                                            <div>
                                                Order ID: {orderId}
                                            </div>
                                        </Typography>

                                        <React.Fragment>
                                            {DisputeForm}
                                        </React.Fragment>
                                    </Paper>

                                    <Paper className={classes.paper}>
                                        <Typography component="h5" variant="h5" align="center">
                                            <div>
                                                Challenge by {address}
                                            </div>
                                        </Typography>
                                        <div className={classes.updiv} style={{ justifyContent: 'space-between' }}>
                                            <div style={{ justifyContent: 'space-evenly' }} className={classes.sidediv}>

                                                {/* Image */}
                                                <div className={classes.updiv}>
                                                    <img src={dispute.initiatorImage}></img>
                                                </div>

                                                {/* Dispute Details */}
                                                <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                                                    <div className={classes.sidediv} style={{ justifyContent: 'space-between' }}>
                                                        <div className={classes.updiv}>
                                                            <div>Title: </div>
                                                            <div>Description: </div>
                                                            <div>Created By: </div>
                                                            <div>Claim Amount: </div>
                                                            <div>Challenge Amount: </div>
                                                        </div>
                                                        <div className={classes.updiv}>
                                                            <div>{dispute.title}</div>
                                                            <div>{dispute.initiatorDescription}</div>
                                                            <div>{dispute.initiatorAddress}</div>
                                                            <div>{dispute.initiatorClaim} DAI</div>
                                                            <div>{dispute.challengeAmount} DAI</div>
                                                        </div>
                                                    </div>
                                                </Typography>
                                            </div>
                                        </div>
                                    </Paper>

                                    <div className={classes.sidediv}>
                                        <div className={classes.submit}>
                                            <RouterLink to='/app/orders'>
                                                <Button
                                                    type="submit"
                                                    variant="outlined"
                                                    color="primary"
                                                    className={classes.hspace}
                                                // onClick={(e) => { onProfileSave(e) }}
                                                >
                                                    View Orders
                                        </Button>
                                            </RouterLink>
                                        </div>
                                        <div className={classes.submit}>
                                            <RouterLink to='/app'>
                                                <Button
                                                    type="submit"
                                                    variant="outlined"
                                                    color="primary"
                                                    className={classes.hspace}
                                                    to={'/app'}
                                                // onClick={(e) => { onProfileSave(e) }}
                                                >
                                                    Go to Dashboard
                                        </Button>
                                            </RouterLink>
                                        </div>
                                        <div className={classes.submit}>
                                            <RouterLink to='/app/products/add'>
                                                <Button
                                                    type="submit"
                                                    variant="outlined"
                                                    color="primary"
                                                    className={classes.hspace}
                                                // onClick={(e) => { onProfileSave(e) }}
                                                >
                                                    Add a product on rent
                                        </Button>
                                            </RouterLink>
                                        </div>
                                    </div>
                                </main>
                            </form>
                        </React.Fragment>
                        :
                        <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                            <div>
                                Unable to fetch dispute for {orderId}. Try again later.
                            </div>
                            <div className={classes.sidediv}>
                                <RouterLink to='/app'>
                                    <Button
                                        type="submit"
                                        variant="outlined"
                                        color="primary"
                                        className={classes.hspace}
                                    // onClick={(e) => { onProfileSave(e) }}
                                    >
                                        Back to Dashboard
                            </Button>
                                </RouterLink>
                            </div>
                        </Typography>
                    :
                    <Typography className={classes.vspace} component="h6" variant="h6" align="center">
                        <div>
                            Oops...Looks like you're lost.
                        </div>
                        <div className={classes.sidediv}>
                            <RouterLink to='/app'>
                                <Button
                                    type="submit"
                                    variant="outlined"
                                    color="primary"
                                    className={classes.hspace}
                                // onClick={(e) => { onProfileSave(e) }}
                                >
                                    Back to Dashboard
                            </Button>
                            </RouterLink>
                        </div>
                    </Typography>
                :
                <Fragment>
                    <div className={classes.loadingbar}></div>
                    <main className={classes.layout}>
                        <Paper className={classes.paper}>
                            <Typography component="h1" variant="h4" align="center">
                                Please connect with a wallet to continue
                            </Typography>
                        </Paper>
                    </main>
                </Fragment>
            }

            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                open={snackbarOpen}
                autoHideDuration={4000}
                onClose={handleClose}
                message={snackbarMessage}
                action={
                    <React.Fragment>
                        <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                            <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment>
                }
            />
        </div>
    );
}

export default ChallengeDispute;