import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    topbar: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between"
    },
    sidediv: {
        display: "flex",
        flexDirection: "row",
        padding: theme.spacing(2),
        overflow: "auto hidden",
        // width: "100%",
        justifyContent: "center",
    },
    updiv: {
        padding: theme.spacing(2),
        display: "flex",
        overflow: "auto",
        flexDirection: "column",
    },
    address: {
        padding: theme.spacing(2)
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(10),
        marginRight: theme.spacing(10),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
            width: 'auto',
            marginLeft: theme.spacing(10),
            marginRight: theme.spacing(10),
        },
    },
    paper: {
        // marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        backgroundColor: theme.palette.background.paper,
        // border: '2px solid #000',
        boxShadow: theme.shadows[5],
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            // marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
            backgroundColor: theme.palette.background.paper,
            // border: '2px solid #000',
            boxShadow: theme.shadows[5],
        },
    },
    spacing: {
        padding: theme.spacing(3, 0, 3),
        spacing: 2
    },
    hspace: {
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3)
    },
    vspace: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3)
    },
    loadingbar: {
        marginTop: theme.spacing(3),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
        },
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    },
    modal: {
        position: 'absolute',
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    button: {
        textDecoration: 'none'
    }
}));