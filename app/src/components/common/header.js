import { useState, useEffect, createContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import { AppBar, Button, Checkbox, IconButton, Paper, Toolbar, Typography } from '@material-ui/core';

import { ethers } from 'ethers';
import InitOnboard from './InitOnboard';
import { Fragment } from 'react';

import Sidebar from './sidebar';

var provider = null;

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    topbar: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between"
    },
    sidediv: {
        display: "flex",
        flexDirection: "row",
        padding: theme.spacing(2)
    },
    updiv: {
        display: "flex",
        flexDirection: "column",
        padding: theme.spacing(2)
    },
    address: {
        padding: theme.spacing(2)
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(10),
        marginRight: theme.spacing(10),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
            width: 'auto',
            marginLeft: theme.spacing(10),
            marginRight: theme.spacing(10),
        },
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
    },
    spacing: {
        padding: theme.spacing(3, 0, 3),
        spacing: 2
    }
}));

function Header(props) {

    const classes = useStyles();

    const [address, setAddress] = useState(null);
    const [wallet, setWallet] = useState(null);
    const [onboard, setOnboard] = useState(null);

    useEffect(() => {
        const onboard = InitOnboard({
            wallet: wallet => {
                setWallet(wallet);
                const ethersProvider = new ethers.providers.Web3Provider(
                    wallet.provider
                );
                provider = ethersProvider;
                props.setEthersProvider(ethersProvider);
                window.localStorage.setItem('selectedWallet', wallet.name);
            },
            address: address => {
                setAddress(address);
                props.setParentAddress(address);
                if (!address) {
                    window.localStorage.removeItem('address');
                } else {
                    window.localStorage.setItem('address', address);
                }
            }

        });
        setOnboard(onboard);
    }, [])

    useEffect(() => {
        const selectedWallet = window.localStorage.getItem('selectedWallet');
        if (selectedWallet && onboard) {
            onboard.walletSelect(selectedWallet);
        }
    }, [onboard])

    async function connectToWallet() {
        if (onboard) {
            await onboard.walletSelect();
            await onboard.walletCheck();
        }
    }

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <Sidebar />
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>
                        DRent
                    </Typography>
                    <div className={classes.address}>
                        {
                            address
                                ? <Button variant="contained" color="primary">{(address.slice(0, 6) + '..' + address.slice(-2))}</Button>
                                : <Button variant="contained" color="primary" onClick={connectToWallet}>Connect to wallet</Button>
                        }
                    </div>
                </Toolbar>
            </AppBar>
        </div>
    );

}

export default Header;