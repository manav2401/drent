import Onboard from 'bnc-onboard';
import { matictestnet } from '../../artifacts/networkIds'

function InitOnboard(subscriptions) {

    const onboard = Onboard({
        networkId: matictestnet,
        networkName: "Matic Mumbai Testnet",
        subscriptions,
        walletSelect: {
            wallets: [
                { walletName: 'metamask' }
            ]
        },
        walletCheck: [
            { checkName: 'connect' },
            { checkName: 'accounts' },
            { checkName: 'network' },
        ]
    });

    return onboard;

}

export default InitOnboard;