import { pinata_api_key, pinata_api_secret } from '../artifacts/pinataKeys';

export function pinIpfs(hash) {

    var myHeaders = new Headers();
    myHeaders.append("pinata_api_key", pinata_api_key);
    myHeaders.append("pinata_secret_api_key", pinata_api_secret);
    myHeaders.append("Content-Type", "application/json");

    fetch("https://api.pinata.cloud/pinning/pinByHash", {
        method: 'POST',
        headers: myHeaders,
        body: JSON.stringify({
            "hashToPin": hash
        }),
    })
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));

}