const Drent = artifacts.require("Drent");
const Dai = artifacts.require("Dai");

module.exports = async function (deployer) {

    await deployer.deploy(Dai, "5777");
    const token = await Dai.deployed();

    await deployer.deploy(Drent, token.address);
};
