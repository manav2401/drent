(function() {
    let express = require("express");
    let cors = require("cors");
    let jwt = require("jsonwebtoken");
    let bodyParser = require("body-parser");
    let mongoClient = require("mongodb").MongoClient;
    let path = require("path");
    let assert = require("assert");
    let morgan = require("morgan");
    let config = require("./config.js");
    let http = require('http');
    let socketIo = require("socket.io");

    let app = express();

    let dbHostUrl = "mongodb://" + config.server + ":" + config.db_port + "/";

    app.use(cors());
    app.use(handleCORSRequests);
    app.use(morgan("common"));
    app.use(
        bodyParser.urlencoded({
            extended: true,
        })
    );
    app.use(bodyParser.json());

    const srv = http.createServer(app);
    const io = socketIo(srv, {
        cors: {
            origin: '*',
        }
    });

    mongoClient.connect(
        dbHostUrl + config.db_name,
        { useUnifiedTopology: true },
        runApp
    );

    srv.listen(config.port, "127.0.0.1");
    console.log("Server starting at http://localhost:" + config.port);

    process.on("SIGINT", killProcess);

    function handleCORSRequests(req, res, next) {
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", "GET, POST");
        res.setHeader(
            "Access-Control-Allow-Headers",
            "X-Requested-With, content-type, Authorization"
        );
        next();
    }

    function killProcess() {
        console.log(".");
        console.log("[success] stopping Node.js server.");
        process.exit();
    }

    function runApp(err, client) {
        assert.strictEqual(
            null,
            err,
            "[failed] establishing connection to mongodb"
        );
        console.log("MongoDB Connected");

        db = client.db(config.db_name);
        let userApi = require("./routes/api.user.js")(app, express, db);
        let productsApi = require("./routes/api.product.js")(app, express, db);
        let orderApi = require("./routes/api.order.js")(app, express, db);
        let disputesApi = require("./routes/api.disputes.js")(app, express, db, io);
        let reputationAPI = require("./routes/api.reputation.js")(app, express, db);

        app.use("/", userApi);
        app.use("/", productsApi);
        app.use("/", orderApi);
        app.use("/", disputesApi);
        app.use("/", reputationAPI);
    }
})();