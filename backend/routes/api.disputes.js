(function (){

    const mongo = require("mongodb");
    const sigUtil = require("eth-sig-util");
    const ethers = require('ethers');
    const Biconomy = require("@biconomy/mexa").Biconomy;

    module.exports = disputesApi;

    function disputesApi(app, express, db, io) {
        let apiRouter = express.Router();

        apiRouter.post("/dispute/create", createDispute);
        apiRouter.get("/disputes", getAllDisputes);
        apiRouter.post("/dispute/status", getDisputePerId);
        apiRouter.get("/dispute/:orderId", getDisputeById);
        apiRouter.put("/dispute/challenge", challengeDispute);
        apiRouter.get("/test", fundsDisperse);
        apiRouter.get("/dispute/status/claim", checkClaimStatus);

        io.on('connection', (socket) => {
            socket.on('chat_message', msg => {
                console.log("Heyy")
                io.emit("chat", "Boht hardd");
            });

            socket.on("getVotes", updatedObj => {
                let disputeId = mongo.ObjectId(updatedObj["_id"])
                let side = updatedObj["side"]
                let address = updatedObj["address"]
                let addressJson = {}
                if(side == "initiatorVotes"){
                    addressJson["initiatorSideAddress"] = address
                }
                else{
                    addressJson["oppositionSideAddress"] = address
                }
                let queryJson = {}
                queryJson[updatedObj["side"]] = 1
                // console.log(queryJson);
                db.collection("disputes").findOneAndUpdate(
                    { "_id": disputeId },
                    {
                        $inc: queryJson,
                        "$push": addressJson
                    },
                    { returnOriginal: false },
                    (err, docs) => {
                        if (err) {
                            console.log("database error")
                            console.log(err);
                        }
                        // console.log("Answer");
                        // console.log(docs);
                        io.emit("change_vote", docs.value);
                    }
                )
            });

            socket.on("claim", requestBody => {
                let address = requestBody["address"];
                let disputeId = mongo.ObjectId(requestBody["_id"]);
                let json = {}
                json["claimAddress"] = address
                db.collection("disputes").findOneAndUpdate(
                    { "_id": disputeId },
                    {
                        "$push": json
                    },
                    { returnOriginal: true },
                    (err, docs) => {
                        if (err) {
                            console.log("database error")
                            console.log(err);
                        }
                        io.emit("claim_change", {message: "Claimed"});
                    }
                )
            });

            socket.on("allVotes", () => {
                var cursor = db.collection("disputes").find();
                cursor.each(processCursor);
                var allDisputes = []
                function processCursor(err, doc){
                    if(err){
                        res.status(500).json({
                            message: "db Error",
                        });
                        return;
                    }

                    if(doc != null) {
                        allDisputes.push(doc);
                    } else{
                        console.log("In");
                        io.emit("get_data", allDisputes);
                    }
                }
            });
        });

        return apiRouter;

        function createDispute(req, res){
            try {
                let disputeBody = req.body
                db.collection("disputes").insertOne(disputeBody, (err, doc) => {
                    if(err){
                        console.log(err);
                        res.status(500).json({ message: "database error" });
                        return;
                    }
                    else{
                        res.status(200).json({ "message": "Dispute created" });
                        return;
                    }
                })
            } catch (err) {
                console.log(err);
                res.status(500).json({ message: "server error" });
                return;
            }
        }

        function getAllDisputes(req, res){
            try {
                var cursor = db.collection("disputes").find();
                cursor.each(processCursor);
                var allDisputes = []
                function processCursor(err, doc){
                    if(err){
                        res.status(500).json({
                            message: "db Error",
                        });
                        return;
                    }

                    if(doc != null) {
                        allDisputes.push(doc);
                    } else{
                        res.status(200).json({
                            response: allDisputes,
                        });
                    }
                }
            } catch (err) {
                console.log(err);
                res.status(500).json({ message: "server error" });
                return;
            }
        }

        function getDisputePerId(req, res){
            try{
                let bodyOrderId = req.body["orderId"];
                let address = req.body["address"];
                db.collection("disputes").findOne({
                    orderId: bodyOrderId
                }, (err, docs) => {
                    if(err){
                        console.log(err);
                        res.status(500).json({ message: "database error" });
                        return;
                    }
                    
                    if(docs == null){
                        res.status(200).json({ message: "Create Dispute", created: false, challenged: false, disabled: false });
                        return;
                    } 
                    else{
                        if(docs["challenged"] == false){
                            if(docs["initiatorAddress"] == address){
                                res.status(200).json({ message: "Create Dispute", created: true, challenged: false, disabled: true });
                                return;
                            }
                            else{
                                res.status(200).json({ message: "Challenge Dispute", created: true, challenged: false, disabled: false });
                                return;
                            }
                        }
                        else{
                            res.status(200).json({ message: "View Dispute", created: true, challenged: true, disabled: false });
                            return;
                        }
                    }
                })
            } catch (err) {
                console.log(err);
                res.status(500).json({ message: "server error" });
                return;
            }
        }

        function getDisputeById(req, res){
            try {
                let bodyOrderId = req.params["orderId"];
                db.collection("disputes").findOne({
                    orderId: bodyOrderId
                }, (err, docs) => {
                    if(err){
                        console.log(err);
                        res.status(500).json({ message: "database error" });
                        return;
                    }

                    if(docs == null){
                        res.status(200).json({ message: "No disputes available for the given order id"});
                        return;
                    }

                    res.status(200).json({data: docs});
                    return;
                })
            } catch (err) {
                console.log(err);
                res.status(500).json({ message: "server error" });
                return;
            }
        }

        function checkClaimStatus(req, res){
            try {
                // console.log("Hello...")
                // console.log(req);
                let bodyOrderId = req.query["orderId"];
                let userAddress = req.query["address"];
                // console.log("hi...", bodyOrderId, userAddress)
                db.collection("disputes").findOne({
                    orderId: bodyOrderId
                }, (err, docs) => {
                    if(err){
                        console.log(err);
                        res.status(500).json({ message: "database error" });
                        return;
                    }

                    if(docs == null){
                        res.status(200).json({ message: "No disputes available for the given order id"});
                        return;
                    }
                    else{
                        console.log("claim list:",docs.claimAddress)
                        if(docs.claimAddress.includes(userAddress)){
                            res.status(200).json({ message: "User has already claimed the funds", vote: true, claimed: true, winner: false, hasVoted: true});
                            return;
                        }
                        else{
                            let winner = null;
                            if(docs.initiatorVotes > docs.oppositionVotes){
                                winner = "initiator";
                            }
                            else{
                                winner = "opposition";
                            }

                            if(docs.initiatorSideAddress.includes(userAddress)){
                                if(winner == "initiator"){
                                    res.status(200).json({ message: "User has already claimed the funds", vote: docs.initiatorAddress,  claimed: false, winner: true, hasVoted: true});
                                    return;
                                }
                                else{
                                    res.status(200).json({ message: "User has already claimed the funds", vote: docs.initiatorAddress,  claimed: false, winner: false, hasVoted: true});
                                    return;
                                }
                            }
                            else if(docs.oppositionSideAddress.includes(userAddress)){
                                if(winner == "initiator"){
                                    res.status(200).json({ message: "User has already claimed the funds", vote: docs.oppositionAddress,  claimed: false, winner: false, hasVoted: true});
                                    return;
                                }
                                else{
                                    res.status(200).json({ message: "User has already claimed the funds", vote: docs.oppositionAddress,  claimed: false, winner: true, hasVoted: true});
                                    return;
                                }
                            }
                            else{
                                res.status(200).json({ message: "User has not votted", vote: false,  claimed: false, winner: false, hasVoted: false});
                                return;
                            }
                        }
                    }
                    
                })

            } catch (err) {
                console.log(err);
                res.status(500).json({ message: "server error" });
                return;
            }
        }

        function challengeDispute(req, res){
            try {
                // console.log(req.body);
                let updatedDispute = req.body;
                console.log(updatedDispute);
                let orderId = req.body["orderId"];
                db.collection("disputes").remove(
                    {orderId: orderId},
                    (err, docs) => {
                        if(err){
                            console.log(err);
                            res.status(500).json({ message: "database error" });
                            return;
                        }
                        
                        db.collection("disputes").insertOne(
                            updatedDispute,
                            (error, document) => {
                                if (error) {
                                    console.log(error);
                                    res.status(500).json({ message: "database error" });
                                    return; 
                                }
                                res.status(200).json({ message: "challenge created" });
                                return;
                            }
                        )


                    }
                )
            } catch (err) {
                console.log(err);
                res.status(500).json({ message: "server error" });
                return;
            }
        }

        async function fundsDisperse(req, res){


            contractABI = [
                {
                    "inputs": [
                        {
                            "internalType": "address",
                            "name": "_trustedForwarder",
                            "type": "address"
                        }
                    ],
                    "stateMutability": "nonpayable",
                    "type": "constructor"
                },
                {
                    "inputs": [],
                    "name": "get",
                    "outputs": [
                        {
                            "internalType": "string",
                            "name": "",
                            "type": "string"
                        }
                    ],
                    "stateMutability": "view",
                    "type": "function"
                },
                {
                    "inputs": [
                        {
                            "internalType": "address",
                            "name": "forwarder",
                            "type": "address"
                        }
                    ],
                    "name": "isTrustedForwarder",
                    "outputs": [
                        {
                            "internalType": "bool",
                            "name": "",
                            "type": "bool"
                        }
                    ],
                    "stateMutability": "view",
                    "type": "function"
                },
                {
                    "inputs": [
                        {
                            "internalType": "string",
                            "name": "_string",
                            "type": "string"
                        }
                    ],
                    "name": "set",
                    "outputs": [
                        {
                            "internalType": "bool",
                            "name": "",
                            "type": "bool"
                        }
                    ],
                    "stateMutability": "nonpayable",
                    "type": "function"
                },
                {
                    "inputs": [],
                    "name": "trustedForwarder",
                    "outputs": [
                        {
                            "internalType": "address",
                            "name": "",
                            "type": "address"
                        }
                    ],
                    "stateMutability": "view",
                    "type": "function"
                }
            ]




            // 8ab8e1e4e5004999b5933a5324f07d2b0bd1c9ad058be4b707ab6d9cf88ce7ed

            let ethersProvider = new ethers.providers.BaseProvider("https://rpc-mumbai.maticvigil.com/");
            // let ethersProvider = new ethers.providers.Web3Provider(window.ethereum);
            // let addr = await ethersProvider.listAccounts();
            // addr = addr[0];

            let biconomy = new Biconomy(ethersProvider,
                { apiKey: "1TBUlj0L8.c83c71bf-d539-4d84-9a9d-4584ba6c19b0", debug: true }
            );
            ethersProvider = new ethers.providers.BaseProvider(biconomy);

            biconomy.onEvent(biconomy.READY, async () => {

                console.log("Hey");
                contractAddress = "0x17Fe61edeA257262676d74B8C214D4592EA347D4";
                
                let userAddress = "0xA7aFEe592B42Fb12a4D0913cF172A6a32Aa00C46";
                let contract = new ethers.Contract(contractAddress, contractABI, biconomy.getSignerByAddress(userAddress));

                let contractInterface = new ethers.utils.Interface(contractABI);
                
                let privateKey = "8ab8e1e4e5004999b5933a5324f07d2b0bd1c9ad058be4b707ab6d9cf88ce7ed";

                let userSigner = new ethers.Wallet(privateKey);

                let functionSignature = contractInterface.encodeFunctionData("set", "abc");

                let rawTx = {
                    to: contractAddress,
                    data: functionSignature,
                    from: userAddress
                };

                let signedTx = await userSigner.signTransaction(rawTx);

                const forwardData = await biconomy.getForwardRequestAndMessageToSign(signedTx);
                console.log(forwardData);

                const signature = sigUtil.signTypedMessage(new Buffer.from(privateKey, 'hex'), { data: forwardData.eip712Format }, 'V3');

                let data = {
                    signature: signature,
                    forwardRequest: forwardData.request,
                    rawTransaction: signedTx,
                    signatureType: biconomy.EIP712_SIGN
                };

                let provider = biconomy.getEthersProvider();
                let txHash = await provider.send("eth_sendRawTransaction", [data]);
                let receipt = await provider.waitForTransaction(txHash);
                // setBiconomy(biconomy);
                // setUserAddress(addr);
                console.log("Biconomy Initialized...User Address:");

            }).onEvent(biconomy.ERROR, (error, message) => {
                console.log("Error:", error, "Message", message);
            })

        }
    }
})();