(function (){

    const mongo = require("mongodb");

    module.exports = reputationApi;

    function reputationApi(app, express, db) {
        let apiRouter = express.Router();

        apiRouter.post("/reputation/update", updateReputation);
        apiRouter.get("/reputation", getReputationByUser);

        return apiRouter;

        function updateReputation(req, res){
            try {
                let address = req.body["address"];
                let event = null;
                if(typeof(req.body["event"]) != "undefined"){
                    event = req.body["event"];
                }
                let voteDecision = null;
                if(typeof(req.body["result"]) != "undefined"){
                    voteDecision = req.body["result"];
                }
                let reputation = null;
                let fetchedReputation = null;
                let count = 0;
                let dividingNumber = 1;

                db.collection("reputation").findOne({address: address}, (err, docs) => {
                    if(err){
                        console.log(err);
                        res.status(500).json({ message: "database error" });
                        return;
                    }
                    if(docs == null){
                        if(event == "rent"){
                            reputation = 2;
                        }
                        else if(event == "borrow"){
                            reputation = 3;
                        }
                        else if(event == "dispute"){
                            reputation = 4;
                        }
                        else if(event == "challenge"){
                            reputation = 4;
                        }
                        else if(event == "vote"){
                            reputation = 2;
                        }
                        else{
                            console.log("Event is undefined : ", event);
                            res.status(500).json({ message: "Event is undefined. Please send proper event" });
                            return;
                        }
                    }
                    else{
                        fetchedReputation = docs["reputation"];
                        count = docs["count"];
                        // console.log("Reputation : ", fetchedReputation);
                        
                        if(count >= 5){
                            dividingNumber = Math.floor(count/5);
                            // console.log("Dividing : ", dividingNumber);
                            if(event == "rent"){
                                reputation = fetchedReputation + 2/dividingNumber;
                            }
                            else if(event == "borrow"){
                                reputation = fetchedReputation + 3/dividingNumber;
                            }
                            else if(event == "dispute"){
                                reputation = fetchedReputation + 4/dividingNumber;
                            }
                            else if(event == "challenge"){
                                reputation = fetchedReputation + 4/dividingNumber;
                            }
                            else if(event == "vote"){
                                reputation = fetchedReputation + 2/dividingNumber;
                            }
                            else if(voteDecision != null){
                                if(voteDecision == "winner"){
                                    reputation = fetchedReputation + 2*0.2;
                                }
                                else{
                                    reputation = fetchedReputation - 2*0.2;
                                }
                            }
                            else{
                                console.log("Event is undefined : ", event);
                                res.status(500).json({ message: "Event is undefined. Please send proper event" });
                                return;
                            }
                        }
                        else{
                            if(event == "rent"){
                                reputation = fetchedReputation + 2;
                            }
                            else if(event == "borrow"){
                                reputation = fetchedReputation + 3;
                            }
                            else if(event == "dispute"){
                                reputation = fetchedReputation + 4;
                            }
                            else if(event == "challenge"){
                                reputation = fetchedReputation + 4;
                            }
                            else if(event == "vote"){
                                reputation = fetchedReputation + 3;
                            }
                            else if(voteDecision != null){
                                if(voteDecision == "winner"){
                                    reputation = fetchedReputation + 2*0.2;
                                }
                                else{
                                    reputation = fetchedReputation - 2*0.2;
                                }
                            }
                            else{
                                console.log("Event is undefined : ", event);
                                res.status(500).json({ message: "Event is undefined. Please send proper event" });
                                return;
                            }
                        }
                    }

                    // let updatedReputationObj = {};
                    // updatedReputationObj["count"] = 1;
                    db.collection("reputation").updateOne(
                        {"address": address},
                        {$set: {"reputation": reputation},
                        $inc: {count: 1}},
                        {upsert: true},
                        (error, document) => {
                            if(error){
                                console.log(err);
                                res.status(500).json({ message: "database error" });
                                return;
                            }
                            res.status(200).json({ message: "Reputation updated", reputation: reputation});
                            return;
                        }
                    )
                })
            } catch (err) {
                console.log(err);
                res.status(500).json({ message: "server error" });
                return;
            }
        }

        function getReputationByUser(req, res){
            try {
                let userAddress = req.query["address"];
                db.collection("reputation").findOne({address: userAddress}, (err, docs) => {
                    if(err){
                        console.log(err);
                        res.status(500).json({ message: "database error" });
                        return;
                    }

                    if(docs == null){
                        res.status(200).json({ message: "No activity found of this user"});
                        return;
                    }

                    res.status(200).json({ reputation: docs.reputation });
                    return;
                })
            } catch (err) {
                console.log(err);
                res.status(500).json({ message: "server error" });
                return;
            }
        }
    }
})();