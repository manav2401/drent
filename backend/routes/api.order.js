(function (){

    const bcrypt = require("bcrypt");
    const config = require("../config");
    const jwt = require("jsonwebtoken");

    module.exports = orderApi;

    function orderApi(app, express, db) {
        let apiRouter = express.Router();

        apiRouter.post("/create/order", createOrder);
        apiRouter.get("/orders", getAllOrders);

        return apiRouter;

        function createOrder(req, res){
            try {
                let orderBody = req.body
                db.collection("orders").insertOne(orderBody, (err, doc) => {
                    if(err){
                        console.log(err);
                        res.status(500).json({ message: "server error" });
                        return;
                    }
                    else{
                        res.status(200).json({ message: "Order created" });
                        return;
                    }
                })
            } catch (err) {
                console.log(err);
                res.status(500).json({ message: "server error" });
                return;
            }
        }

        function getAllOrders(req, res){
            try {
                var cursor = db.collection("orders").find();
                cursor.each(processCursor);
                var allProducts = []
                function processCursor(err, doc){
                    if(err){
                        res.status(500).json({
                            message: "db Error",
                        });
                        return;
                    }
    
                    if(doc != null) {
                        allProducts.push(doc);
                    } else{
                        res.status(200).json({
                            response: allProducts,
                        });
                    }
                }
            } catch (err) {
                console.log(err);
                res.status(500).json({ message: "server error" });
                return;
            }
        }
    }
    
})();