(function (){

    const bcrypt = require("bcrypt");
    const config = require("../config");
    const jwt = require("jsonwebtoken");

    module.exports = userApi;

    function userApi(app, express, db) {
        let apiRouter = express.Router();

        apiRouter.post("/user/create/profile", createProfile);

        return apiRouter;

        function createProfile(req, res){
            try {
                let userBody = req.body
                db.collection("user").insertOne(userBody, (err, doc) => {
                    if(err){
                        console.log(err);
                        res.status(500).json({ message: "server error" });
                        return;
                    }
                    else{
                        res.status(200).json({ "message": "Profile created" });
                        return;
                    }
                })
            } catch (err) {
                console.log(err);
                res.status(500).json({ message: "server error" });
                return;
            }
        }
    }
})();