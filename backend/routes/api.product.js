(function (){

    const bcrypt = require("bcrypt");
    const config = require("../config");
    const jwt = require("jsonwebtoken");
    const mongo = require("mongodb");

    module.exports = productsApi;

    function productsApi(app, express, db) {
        let apiRouter = express.Router();

        apiRouter.post("/addproduct", addProduct);
        apiRouter.get("/products", getAllProducts);
        apiRouter.get("/products/:address", getProductsOfUser);

        return apiRouter;

        function addProduct(req, res){
            try {
                let productBody = req.body
                db.collection("products").insertOne(productBody, (err, doc) => {
                    if(err){
                        console.log(err);
                        res.status(500).json({ message: "server error" });
                        return;
                    }
                    else{
                        db.collection("productmap").updateOne(
                            {"owner": productBody["owner"]},
                            {$addToSet: {map: productBody._id}}, {upsert: true},
                            (error, docs) => {
                                if(error){
                                    console.log(error);
                                    res.status(500).json({ message: "product map server error" });
                                    return;
                                }
                                else{
                                    res.status(200).json({ message: "product added" });
                                    return;
                                }
                            })
                    }
                })
            } catch (err) {
                console.log(err);
                res.status(500).json({ message: "server error" });
                return;
            }
        }

        function getAllProducts(req, res){
            try {
                var cursor = db.collection("products").find();
                cursor.each(processCursor);
                var allProducts = []
                function processCursor(err, doc){
                    if(err){
                        res.status(500).json({
                            message: "db Error",
                        });
                        return;
                    }

                    if(doc != null) {
                        allProducts.push(doc);
                    } else{
                        res.status(200).json({
                            response: allProducts,
                        });
                    }
                }
            } catch (err) {
                console.log(err);
                res.status(500).json({ message: "server error" });
                return;
            }
        }

        function getProductsOfUser(req, res){
            try {
                userAddress = req.params.address;
                db.collection("productmap").findOne({"owner": userAddress}, (err, doc) => {
                    if(err){
                        console.log(err);
                        res.status(500).json({ message: "server error" });
                        return;
                    }
                    else{
                        let productIdArray = doc;
                        console.log(productIdArray);
                        // productIdArray = productIdArray["map"].map(s => mongo.ObjectId(s));
                        // console.log(mongo.ObjectId(productIdArray[0]));
                        var cursor = db.collection("products").find({"_id": {$in: productIdArray["map"]}});
                        cursor.each(processCursor);
                        var answer = []
                        function processCursor(err, doc){
                            if(err){
                                res.status(500).json({
                                    message: "db Error",
                                });
                                return;
                            }
        
                            if(doc != null) {
                                answer.push(doc);
                            } else{
                                res.status(200).json({
                                    response: answer,
                                });
                            }
                        }
                    }
                })
            } catch (err) {
                console.log(err);
                res.status(500).json({ message: "server error" });
                return;
            }
        }
    }
})();