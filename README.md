### Drent: A Decentralized C2C Marketplace

#### Overview
- Drent is a decentralized C2C rent marketplace with almost no platform fees. The platform aims at solving the problem of optimal resource utilization by providing flexibility for almost any user to rent out their unused products and others to borrow them instead of investing a high amount in buying them.

- It solves some of the shortcomings of a centralized B2C marketplace such as not allowing users to rent their products or complete governance of a single party, and lack of transparency.

#### Design / Description
- Major goal is to create an autonomous platform, which works on crowd sourced decisions.
- Drent is divided into 2 major parts, one being the renting functionalities and the other being an autonomous dispute resolving mechanism.
- The renting functionalities involves renting & borrowing of products along with their reputation with each event.
- For a marketplace, involving no middleman, a mechanism to govern and resolve disputes with fund management is required.

#### Specifications
##### Rentals
- Users can create/update their profile
- After profile creation, users can add / borrow product on rent.
- While borrowing, the user needs to pay collateral fees along with the rental fees, which are locked in an escrow contract.
- Once the order is finished, users get their collateral back

##### Dispute Resolving Mechanism
- Any party involved in order (owner or borrower) can call for a dispute in case of discrepancies among them.
- Let's say Alice & Bob are the 2 actors involved where Alice is the owner and Bob is the borrower.
- While placing the order, Bob will pay the rent price (depending on the number of days) let's say `a` DAI and the collateral amount let's say `b` DAI.
- These funds are locked in an escrow contract. (Has scope of improvement by integrating Aave for generating interest on those funds).
- While the order is running, the borrower can create a dispute against the owner & on completion the owner can create a dispute against the borrower.
- Alice created a dispute against Bob after completion of the order & claimed `c` DAI's (`0 < c <= b`) from bob for damage/loss of the product (in case any). Alice in this case has to pay a fee for creating a dispute, which has no restrictions on it. Alice can provide written as well as photographic proofs for the dispute.
- Bob has 24 hrs to challenge this dispute by paying the exact same amount of fees and state his claim let's say `d` DAI (`0 <= d < c`) against Alice. Bob can also provide descriptive and photographic proofs.
- In case of Bob creating a dispute (for fake or poor quality product) the claim would be `d` DAI (`0 ≤ d < a`) & Alice's claim would be `c` DAI (`d < c ≤ a`).
- This begins the registration period of 7 days and the dispute is listed publicly for others to vote by looking at the claims.
- At the end of the registration period, the result is announced on the platform and those who voted for the losing party lose 20% of their stake which is distributed as a reward among those who voted winning party.
- If Alice wins, she gets the claimed `c` DAI's from the collateral locked by Bob in the escrow contract along with the rent fees & Bob gets the remaining collateral (which will be `b-c` DAI) back.
- If Bob wins, Alice gets `c-d` DAI's (`c - d = 0` is possible)from the collateral locked by Bob in the escrow contract along with the rent fees & Bob gets the remaining collateral (which will be `b-d` DAI) back.
- Both of them gets their challenge amount back (can be improved by including reward & punishment).

##### Reputation Mechanism
- Based on user events & interactions, the reputation of the user changes (formulated by historical data).
- This reputation mechanism will in turn help gaining more users who can be trusted & depending on the reputation discount on collateral amount can be given.
- The events affecting reputation involves renting / borrowing product, creating / challenging dispute, voting & winning / losing the dispute.
- Can be improvised with time for weighted reputation & better reward / punishment mechanism.

#### Technicalities
- The contracts are deployed on Matic Network for Lightning fast transactions & lower gas fees.
- Gasless Meta Transactions (for storage calls) powered by Biconomy.
- Distributed storage powered by IPFS.
- Interest generation on tokens powered by Aave Protocol on Matic (partially implemented)

#### Demo
- The demo is available [here](https://youtu.be/cur3NuhdQ-w).