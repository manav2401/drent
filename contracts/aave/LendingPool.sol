// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <=0.8.0;

abstract contract LendingPool {
    function deposit(address _reserve, uint256 _amount, uint16 _referralCode) public virtual;
}