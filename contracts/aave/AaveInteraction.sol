// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <=0.8.0;

import { LendingPool } from "./LendingPool.sol";

interface IERC20 {
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function allowance(address owner, address spender) external view returns (uint256);
    function transfer(address recipient, uint256 amount) external returns (bool);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

contract AaveInteraction {
    
    LendingPool aaveLendingPool;
    // 0x580D4Fdc4BF8f9b5ae2fb9225D584fED4AD5375c on kovan
    
    // AToken aaveToken;
    // 0x58AD4cB396411B691A9AAb6F74545b2C5217FE6a on kovan
    
    address lendingPoolCoreAddress;
    // 0x95D1189Ed88B380E319dF73fF00E479fcc4CFa45 on kovan
    
    address daiAddress;
    // 0xFf795577d9AC8bD7D90Ee22b6C1703490b6512FD on kovan

    constructor(address _lendingPoolAddress, address _lendingPoolCoreAddress, address _daiAddress) {
        aaveLendingPool = LendingPool(_lendingPoolAddress);
        // aaveToken = AToken(_aDaiToken);
        lendingPoolCoreAddress = _lendingPoolCoreAddress;
        daiAddress = _daiAddress;
    }
    
    event Deposited(address _reserve, uint256 _amount);
    event Redeemed(address _reserve, uint256 _amount);
    
    function depositToAave(uint256 _amount, uint16 _referralCode) public payable {
        
        // approve
        IERC20(daiAddress).approve(lendingPoolCoreAddress, _amount);
        
        // deposit
        aaveLendingPool.deposit(daiAddress, _amount, _referralCode);
        
        // emit event
        emit Deposited(daiAddress, _amount);
        
    }
    
    // function withdrawFromAave(uint256 _amount) public {
        
    //     // redeem
    //     aaveToken.redeem(_amount);
        
    //     // emit event
    //     emit Redeemed(daiAddress, _amount);
        
    // }
    
}