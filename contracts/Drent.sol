// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <=0.8.0;

pragma experimental ABIEncoderV2;

import "./BaseRelayRecipient.sol";

interface IERC20 {
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function allowance(address owner, address spender) external view returns (uint256);
    function transfer(address recipient, uint256 amount) external returns (bool);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

/** 
* @title Contrat for Drent, a C2C renting platform 
* @author Manav Darji
*/
contract Drent is BaseRelayRecipient {
    
    event ProfileAdded(address _address, string _hash);
    event ProductAdded(address _address, string _hash);
    event OrderAdded(address _owner, address _borrower, string _hash);
    event OrderUpdated(address _owner, address _borrower, string _hash);
    event FundsAdded(address _address, uint _amount);
    event FundsDispersed(address _address, uint _amount);

    IERC20 private token;

    constructor(IERC20 _token, address _trustedForwarder) {
        token = _token;
        trustedForwarder = _trustedForwarder;
    }

    // user address to user profile hash mapping
    mapping(address => string) public profileByUser;

    // array of product hash
    string[] public products;

    // user address to product hash mapping
    mapping(address => string[]) public productsByUser;

    // product hash to user address mapping
    mapping(string => address) public userByProduct;
    
    // product hash to availability mapping
    mapping(string => bool[]) public availabilityByProduct;

    // user address to order hash mapping
    mapping(address => string[]) public ordersByUser;

    // user address to funds locked mapping
    mapping(address => uint) public balances;

    // add user profile
    function addUser(string memory _hash) external {
        profileByUser[_msgSenderForwarder()] = _hash;
        emit ProfileAdded(_msgSenderForwarder(), _hash);
    }

    // modifier to check existance of profile
    modifier doProfileExist(address _address) {
        require(keccak256(abi.encodePacked(profileByUser[_address])) != keccak256(abi.encodePacked("")), "No profile exists");
        _;
    }

    function getUser() public view doProfileExist(msg.sender) returns(string memory) {
        return profileByUser[msg.sender];
    }

    function addProduct(string memory _hash, bool[] memory _availability) public doProfileExist(_msgSenderForwarder()) {
        products.push(_hash);
        productsByUser[_msgSenderForwarder()].push(_hash);
        userByProduct[_hash] = _msgSenderForwarder();
        availabilityByProduct[_hash] = _availability;
        emit ProductAdded(_msgSenderForwarder(), _hash);
    }

    function getAllProducts() public view returns (string[] memory) {
        return products;
    }

    function getUserProducts() public view returns(string[] memory) {
        return productsByUser[msg.sender];
    }

    function getProductOwner(string memory _hash) public view returns(address) {
        return userByProduct[_hash];
    }
    
    function getProductAvailability(string memory _hash) public view returns(bool[] memory) {
        return availabilityByProduct[_hash];
    }

    function addOrder(address _address, string memory _hash) internal returns(bool) {
        ordersByUser[_address].push(_hash);
        return true;
    }

    function getUserOrders() public view returns(string[] memory) {
        return ordersByUser[msg.sender];
    }

    function placeOrder(string memory _hash, address _owner, string memory _productHash, bool[] memory _availability) public payable doProfileExist(msg.sender) {
        
        // transfer funds
        token.transferFrom(msg.sender, address(this), msg.value);

        // change balance of user
        balances[msg.sender] += msg.value;

        // add order in owner
        addOrder(_owner, _hash);
        
        // add order in borrower
        addOrder(msg.sender, _hash);
        
        // update product availability mapping
        availabilityByProduct[_productHash] = _availability;
        
        emit OrderAdded(_owner, msg.sender, _hash);
    }

    function sendFunds(address _address, uint _amount) public {

        // transfer funds
        token.transfer(_address, _amount);
        
        FundsDispersed(_address, _amount);

    }
    
    function acceptFunds() public payable {
        
        // transfer funds
        token.transferFrom(msg.sender, address(this), msg.value);
        
        // change balance of user
        balances[msg.sender] += msg.value;
        
        emit FundsAdded(msg.sender, msg.value);
        
    }

    function getUserBalances() public view returns(uint) {
        return balances[msg.sender];
    }

}
